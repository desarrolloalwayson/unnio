function validarIngreso(){
  var emailIngreso;
  var passwordIngreso;
  var expresionEmail;
  var mensajeError;

  emailIngreso             = document.getElementById("emailIngreso").value;
  passwordIngreso          = document.getElementById("passwordIngreso").value;

  expresionEmail     = /\w+@\w+\.+[a-z]/;

  //email.setCustomValidity("¡Yo esperaba una dirección de correo!");

//Validación Campo Email
  if(emailIngreso == null || emailIngreso.length == 0 ){
    document.getElementById("mensajeErrorEmailLogin").style.display = 'block';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "*ERROR: El campo Email no puede ir vacío.";
    return false;
  }else {
    document.getElementById("mensajeErrorEmailLogin").style.display = 'none';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "";
  }

  if(!expresionEmail.test(emailIngreso)){
    document.getElementById("mensajeErrorEmailLogin").style.display = 'block';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "*ERROR: El correo ingresado no es válido.";
    return false;
  }else {
    document.getElementById("mensajeErrorEmailLogin").style.display = 'none';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "";
  }

  if(emailIngreso.length>100){
    document.getElementById("mensajeErrorEmailLogin").style.display = 'block';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "*ERROR: El correo ingresado es muy largo.";
    email.setCustomValidity("¡Yo esperaba una dirección de correo!");
    return false;
  }else {
    document.getElementById("mensajeErrorEmailLogin").style.display = 'none';
    document.getElementById("mensajeErrorEmailLogin").innerHTML     = "";
  }

//Validación Campo Contraseña
  if(passwordIngreso == null || passwordIngreso.length == 0){
      document.getElementById("mensajeErrorContrasenaLogin").style.display = 'block';
      document.getElementById("mensajeErrorContrasenaLogin").innerHTML     = "*ERROR: El campo Contraseña no puede ir vacío.";
			return false;
	}else {
    document.getElementById("mensajeErrorContrasenaLogin").style.display = 'none';
    document.getElementById("mensajeErrorContrasenaLogin").innerHTML     = "";
    window.location.href = "home.html";
  }

  if(passwordIngreso.length>20){
    document.getElementById("mensajeErrorContrasenaLogin").style.display = 'block';
    document.getElementById("mensajeErrorContrasenaLogin").innerHTML     = "*ERROR: La contraseña es muy larga.";
    return false;
  }else {
    document.getElementById("mensajeErrorContrasenaLogin").style.display = 'none';
    document.getElementById("mensajeErrorContrasenaLogin").innerHTML     = "";
  }




}
