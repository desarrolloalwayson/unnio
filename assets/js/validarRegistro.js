function validarRegistro(){
  var nombresRegistro;
  var apellidosRegistro;
  var emailRegistro;
  var telefonoRegistro;
  var passwordRegistro;
  var confirmarPasswordRegistro;

  var expresionEmail;
  var expresionNombres;

  //var mensajeError;

  nombresRegistro           = document.getElementById("nombresRegistro").value;
  apellidosRegistro         = document.getElementById("apellidosRegistro").value;
  emailRegistro             = document.getElementById("emailRegistro").value;
  telefonoRegistro          = document.getElementById("telefonoRegistro").value;
  passwordRegistro          = document.getElementById("passwordRegistro").value;
  confirmarPasswordRegistro = document.getElementById("confirmarPasswordRegistro").value;

  expresionNombres   = /^\s+$/;
  expresionApellidos = /^\s+$/;
  expresionEmail     = /\w+@\w+\.+[a-z]/;

  // mensajeError = document.getElementById("mensajeError").style.display = 'none';

  // if(nombresRegistro === "" || apellidosRegistro === "" || emailRegistro === "" ||
  // telefonoRegistro === "" || passwordRegistro === "" || confirmarPasswordRegistro === ""){
  //   alert("Los campos no pueden estar vacios.");
  //
  //   document.getElementById("nombresRegistro").className +=' error';
  //   document.getElementById("apellidosRegistro").className +=' error';
  //   document.getElementById("emailRegistro").className +=' error';
  //   document.getElementById("telefonoRegistro").className +=' error';
  //   document.getElementById("passwordRegistro").className +=' error';
  //   document.getElementById("confirmarPasswordRegistro").className +=' error';
  //
  //   return false;
  //
  // }else{
  //   document.getElementById("nombresRegistro").className='input';
  //   document.getElementById("apellidosRegistro").className='input';
  //   document.getElementById("emailRegistro").className='input';
  //   document.getElementById("telefonoRegistro").className='input';
  //   document.getElementById("passwordRegistro").className='input';
  //   document.getElementById("confirmarPasswordRegistro").className='input';
  // }


//Validación Campo Nombres
  if(nombresRegistro == null || nombresRegistro.length == 0 || expresionNombres.test(nombresRegistro) ){
    document.getElementById("mensajeErrorNombres").style.display = 'block';
    document.getElementById("mensajeErrorNombres").innerHTML     = "*ERROR: El campo Nombres no puede ir vacío.";
		return false;
	}else {
    document.getElementById("mensajeErrorNombres").style.display = 'none';
    document.getElementById("mensajeErrorNombres").innerHTML     = "";
  }

  if(nombresRegistro.length>50){
    document.getElementById("mensajeErrorNombres").style.display = 'block';
    document.getElementById("mensajeErrorNombres").innerHTML     = "*ERROR: Los Nombres son muy largos.";
    return false;
  }else {
    document.getElementById("mensajeErrorNombres").style.display = 'none';
    document.getElementById("mensajeErrorNombres").innerHTML     = "";
  }

//Validación Campo Apellidos
  if(apellidosRegistro == null || apellidosRegistro.length == 0 || expresionApellidos.test(apellidosRegistro) ){
      document.getElementById("mensajeErrorApellidos").style.display = 'block';
      document.getElementById("mensajeErrorApellidos").innerHTML     = "*ERROR: El campo Apellidos no puede ir vacío.";
			return false;
	}else {
    document.getElementById("mensajeErrorApellidos").style.display = 'none';
    document.getElementById("mensajeErrorApellidos").innerHTML     = "";
  }

  if(apellidosRegistro.length>80){
    document.getElementById("mensajeErrorApellidos").style.display = 'block';
    document.getElementById("mensajeErrorApellidos").innerHTML     = "*ERROR: Los Apellidos son muy largos.";
    return false;
  }else {
    document.getElementById("mensajeErrorApellidos").style.display = 'none';
    document.getElementById("mensajeErrorApellidos").innerHTML     = "";
  }

//Validación Campo Email
  if(emailRegistro == null || emailRegistro.length == 0 ){
    document.getElementById("mensajeErrorEmail").style.display = 'block';
    document.getElementById("mensajeErrorEmail").innerHTML     = "*ERROR: El campo Email no puede ir vacío.";
    return false;
  }else {
    document.getElementById("mensajeErrorEmail").style.display = 'none';
    document.getElementById("mensajeErrorEmail").innerHTML     = "";
  }

  if(!expresionEmail.test(emailRegistro)){
    document.getElementById("mensajeErrorEmail").style.display = 'block';
    document.getElementById("mensajeErrorEmail").innerHTML     = "*ERROR: El correo ingresado no es válido.";
    return false;
  }else {
    document.getElementById("mensajeErrorEmail").style.display = 'none';
    document.getElementById("mensajeErrorEmail").innerHTML     = "";
  }

  if(emailRegistro.length>100){
    document.getElementById("mensajeErrorEmail").style.display = 'block';
    document.getElementById("mensajeErrorEmail").innerHTML     = "*ERROR: El correo ingresado es muy largo.";
    return false;
  }else {
    document.getElementById("mensajeErrorEmail").style.display = 'none';
    document.getElementById("mensajeErrorEmail").innerHTML     = "";
  }

//Validación Campo Teléfono
  if( telefonoRegistro == null || telefonoRegistro.length == 0 ){
     document.getElementById("mensajeErrorTelefono").style.display = 'block';
     document.getElementById("mensajeErrorTelefono").innerHTML     = "*ERROR: El campo Teléfono no puede ir vacío.";
	   return false;
  }else {
    document.getElementById("mensajeErrorTelefono").style.display = 'none';
    document.getElementById("mensajeErrorTelefono").innerHTML     = "";
  }

  if(isNaN(telefonoRegistro)){
    document.getElementById("mensajeErrorTelefono").style.display = 'block';
    document.getElementById("mensajeErrorTelefono").innerHTML     = "*ERROR: El teléfono ingresado no es un número válido.";
    return false;
  }else {
    document.getElementById("mensajeErrorTelefono").style.display = 'none';
    document.getElementById("mensajeErrorTelefono").innerHTML     = "";
  }

  if(telefonoRegistro.length>12){
    document.getElementById("mensajeErrorTelefono").style.display = 'block';
    document.getElementById("mensajeErrorTelefono").innerHTML     = "*ERROR: El teléfono ingresado es muy largo.";
    return false;
  }else {
    document.getElementById("mensajeErrorTelefono").style.display = 'none';
    document.getElementById("mensajeErrorTelefono").innerHTML     = "";
  }

//Validación Campo Contraseña
  if(passwordRegistro == null || passwordRegistro.length == 0){
      document.getElementById("mensajeErrorContrasena").style.display = 'block';
      document.getElementById("mensajeErrorContrasena").innerHTML     = "*ERROR: El campo Contraseña no puede ir vacío.";
			return false;
	}else {
    document.getElementById("mensajeErrorContrasena").style.display = 'none';
    document.getElementById("mensajeErrorContrasena").innerHTML     = "";
  }

  if(passwordRegistro.length>20){
    document.getElementById("mensajeErrorContrasena").style.display = 'block';
    document.getElementById("mensajeErrorContrasena").innerHTML     = "*ERROR: La contraseña es muy larga.";
    return false;
  }else {
    document.getElementById("mensajeErrorContrasena").style.display = 'none';
    document.getElementById("mensajeErrorContrasena").innerHTML     = "";
  }

//Validación Campo Confirmar Contraseña
  if(confirmarPasswordRegistro == null || confirmarPasswordRegistro.length == 0){
      document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'block';
      document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "*ERROR: El campo Confirmar Contraseña no puede ir vacío.";
			return false;
	}else {
    document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'none';
    document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "";
  }

  if(confirmarPasswordRegistro != passwordRegistro){
    document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'block';
    document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "*ERROR: La contraseña debe ser igual al campo anterior.";
    return false;
  }else {
    document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'none';
    document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "";
  }

  if(confirmarPasswordRegistro.length>20){
    document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'block';
    document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "*ERROR: La contraseña es muy larga.";
    return false;
  }else {
    document.getElementById("mensajeErrorContrasenaConfirm").style.display = 'none';
    document.getElementById("mensajeErrorContrasenaConfirm").innerHTML     = "";
  }




}
