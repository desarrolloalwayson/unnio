/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : seduc_asistencia

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-12-10 09:52:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for carga
-- ----------------------------
DROP TABLE IF EXISTS `carga`;
CREATE TABLE `carga` (
  `rut` varchar(12) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of carga
-- ----------------------------
INSERT INTO `carga` VALUES ('17.337.253-1');
INSERT INTO `carga` VALUES ('10.641.351-7');
INSERT INTO `carga` VALUES ('10.466.315-K');
INSERT INTO `carga` VALUES ('9.133.905-6');

-- ----------------------------
-- Table structure for cuentas
-- ----------------------------
DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE `cuentas` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `banco` varchar(30) DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `estado` enum('0','1') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cuentas
-- ----------------------------

-- ----------------------------
-- Table structure for destinatarios
-- ----------------------------
DROP TABLE IF EXISTS `destinatarios`;
CREATE TABLE `destinatarios` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of destinatarios
-- ----------------------------
INSERT INTO `destinatarios` VALUES ('1', 'Oscar1', 'Cornejo1', 'ocornejo1@alwayson.es', '12345671', '0', null);
INSERT INTO `destinatarios` VALUES ('8', 'Oscar', 'Cornejo', 'ocornejo@alwayson.es', '1234567', '1', '1');
INSERT INTO `destinatarios` VALUES ('9', 'destinatario test', 'apellido test', 'correo@correo.cl', '12345677', '1', '3');

-- ----------------------------
-- Table structure for dispositivos
-- ----------------------------
DROP TABLE IF EXISTS `dispositivos`;
CREATE TABLE `dispositivos` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(30) DEFAULT NULL,
  `marca` varchar(30) DEFAULT NULL,
  `modelo` varchar(30) DEFAULT NULL,
  `codigo` varchar(30) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dispositivos
-- ----------------------------
INSERT INTO `dispositivos` VALUES ('1', 'celular', 'LG', '1234', '123de34', '1', '1');

-- ----------------------------
-- Table structure for inversiones
-- ----------------------------
DROP TABLE IF EXISTS `inversiones`;
CREATE TABLE `inversiones` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(30) DEFAULT NULL,
  `monto` int(15) DEFAULT NULL,
  `detalle` varchar(30) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inversiones
-- ----------------------------
INSERT INTO `inversiones` VALUES ('1', 'aaa', '1000', 'test', '1', null);

-- ----------------------------
-- Table structure for otros
-- ----------------------------
DROP TABLE IF EXISTS `otros`;
CREATE TABLE `otros` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(30) DEFAULT NULL,
  `monto` int(15) DEFAULT NULL,
  `detalle` varchar(50) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of otros
-- ----------------------------

-- ----------------------------
-- Table structure for pasivos
-- ----------------------------
DROP TABLE IF EXISTS `pasivos`;
CREATE TABLE `pasivos` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(30) DEFAULT NULL,
  `mes` varchar(20) DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `monto` int(15) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pasivos
-- ----------------------------

-- ----------------------------
-- Table structure for redes
-- ----------------------------
DROP TABLE IF EXISTS `redes`;
CREATE TABLE `redes` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `red` varchar(20) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `email_personal` varchar(30) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of redes
-- ----------------------------

-- ----------------------------
-- Table structure for seguros
-- ----------------------------
DROP TABLE IF EXISTS `seguros`;
CREATE TABLE `seguros` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `entidad` varchar(30) DEFAULT NULL,
  `detalle` varchar(50) DEFAULT NULL,
  `estado` enum('1','0') DEFAULT '1',
  `id_usuario` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seguros
-- ----------------------------

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `rut` varchar(12) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `estado` enum('0','1') DEFAULT '1',
  `npassword` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'Gabriel Esteban', 'Moya Monsalve', '16.715.630-4', 'gmoya@alwayson.cl', '12345678', '81dc9bdb52d04dc20036dbd8313ed055', '1', 'UqlmnAEQEQ');
INSERT INTO `usuarios` VALUES ('2', 'Soporte', 'Testing', '11.111.111-1', 'soporte@alwayson.cl', '87654321', '81dc9bdb52d04dc20036dbd8313ed055', '1', 'UqlmnAEQEQ');
