<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cobertura extends CI_Controller {

	public function __construct(){	
	parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
	$this->load->library('parser');
    $this->load->model('MyModel');
	$this->init();	
	}
	public function init(){
        if(empty($this->session->userdata('m_email'))){
            redirect(base_url('home/login'),'refresh');
            exit();
        }    
    }

	public function index(){
		$id_user = $this->session->userdata('m_id');
		$this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('estado','1');
        $this->db->where('id',$id_user);     
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $datos = $query->result_array();

        $data['datos'] = $datos;        
		$this->load->view('legalDetalle',$data);

	}
}