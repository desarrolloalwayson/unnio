<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

	public function __construct(){	
	parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
	$this->load->library('parser');
    //$this->load->model('MyModel');
	$this->init();
	}
	public function init(){
        if(empty($this->session->userdata('m_email'))){
            redirect(base_url('home/login'),'refresh');
            exit();
        }    
    }

	public function prevencion(){
		$this->load->view('prevencion');
	}
	public function alertaParental(){
		$this->load->view('alertaParental');
	}
	public function investigacion(){
		$this->load->view('investigacion');
	}
	public function accion(){
		$this->load->view('accion');
	}

	public function cobertura(){
		$this->load->view('legalDetalle');
	}

}