<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){	
	parent::__construct();
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
    $this->load->library('parser');
    $this->load->model('MyModel');
    $this->load->library('general');
	}
	public function index(){
		$this->load->view('home');
	}
	public function login(){

		if(!empty($this->session->userdata('m_nombres'))){
			redirect(base_url('home/'),'refresh');
        	exit();
        	echo 'la sesion existe';
		}elseif ($this->input->post('memail')) {			
			$email = $this->input->post('memail');
        	$password = $this->input->post('mpassword');
			$valido = $this->MyModel->login($email,$password);
			if (!empty($valido)){
				$sessiondata = array(
                  'm_id' => $valido[0]['id'],
                  'm_nombres' => $valido[0]['nombres'],
                  'm_apellidos' => $valido[0]['apellidos'],
                  'm_rut' => $valido[0]['rut'], 
                  'm_email' => $valido[0]['email'],
                  'm_telefono' => $valido[0]['telefono']
                );
            	$this->session->set_userdata($sessiondata);
	        	$this->session->set_flashdata('msje_login', '1');
        		redirect(base_url('home/'),'refresh');
        		exit();
        		//cho 'se creo la sesion sin problemas';
			}else{
				$this->session->set_flashdata('msje_login', '0');
				redirect(base_url('home/login'),'refresh');
				exit();
				//echo 'pass no coinciden';
			}

		}else{
			//echo 'pantalla login';
			$this->load->view('login');
		}	
		
	}
	public function registro_modal(){
		$this->load->view('modal/registro');
	}
	public function registro_guardar(){
    $vrut = $this->input->post('mrut');
    $valido = $this->MyModel->verificar_rut($vrut);
      if (!($valido)){
          $nuevo_destinatario = array(
                'nombres' => $this->input->post('mnombre'),
                'apellidos' => $this->input->post('mapellido'),
                'rut' => $this->input->post('mrut'),
                'email' => $this->input->post('memail1'),
                'telefono' => $this->input->post('mtelefono'),
                'password' => md5($this->input->post('mpassword1')),
                'npassword' => $this->general->encrypt($this->input->post('mpassword1'))
            );
          $destinatario = $this->MyModel->agregar_model('usuarios',$nuevo_destinatario);
                
          if($destinatario>0 || $destinatario!=''){
            $sessiondata = array(
                    'm_id' => $destinatario,
                    'm_nombres' => $this->input->post('mnombre'),
                    'm_apellidos' => $this->input->post('mapellido'),
                    'm_email' => $this->input->post('memail1'),
                    'm_telefono' => $this->input->post('mtelefono')
                  );
                $this->session->set_userdata($sessiondata);
              //redirecciono  
            redirect(base_url('home/'));
          }
      }else{
        $this->session->set_flashdata('msje_login', '2');
        redirect(base_url('home/login'),'refresh');
      }
	}
}
