<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ActivosPersonales extends CI_Controller {

	public function __construct(){	
	parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
	$this->load->library('parser');
    $this->load->model('MyModel');
	$this->init();
    $this->load->library('general');
	}
    
    public function init(){
        if(empty($this->session->userdata('m_email'))){
            redirect(base_url('home/login'),'refresh');
            exit();
        }    
    }
    


	public function index(){

        $id_usuario = $this->session->userdata('m_id');

        $this->db->select('*');
        $this->db->from('destinatarios');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $destinatarios = $query->result_array();

        $this->db->select('*');
        $this->db->from('dispositivos');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);   
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $dispositivos = $query->result_array();

        $this->db->select('*');
        $this->db->from('inversiones');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);      
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $inversiones = $query->result_array();

        $this->db->select('*');
        $this->db->from('seguros');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);  
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $seguros = $query->result_array();

        $this->db->select('*');
        $this->db->from('cuentas');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $cuentas = $query->result_array();

        $this->db->select('*');
        $this->db->from('otros');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $otros = $query->result_array();

        $this->db->select('*');
        $this->db->from('pasivos');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $pasivos = $query->result_array();

        $this->db->select('*');
        $this->db->from('redes');
        $this->db->where('estado','1');
        $this->db->where('id_usuario',$id_usuario);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $redes = $query->result_array();

        $data['redes'] = $redes;
        $data['pasivos'] = $pasivos;
        $data['cuentas'] = $cuentas;
        $data['seguros'] = $seguros;
        $data['inversiones'] = $inversiones;
        $data['destinatarios'] = $destinatarios;
        $data['dispositivos'] = $dispositivos;
        $data['otros'] = $otros;
		$this->load->view('activos',$data);
	}
	public function agregar_destinatarios(){
    
	}
    public function agregarDestinatario_guardar(){
            $nuevo_destinatario = array(
                
                'nombres' => $this->input->post('mnombres'),
                'apellidos' => $this->input->post('mapellidos'),
                'email' => $this->input->post('memail'),
                'telefono' => $this->input->post('mtelefono'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('destinatarios',$nuevo_destinatario);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su destinatario se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'paneldestinatario');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_destinatario($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);  
        $update_destinatarios = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('destinatarios',$update_destinatarios,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su destinatario se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'paneldestinatario');
        redirect(base_url('activospersonales/'));      
    }
    public function agregarDispositivo_guardar(){            
            $nuevo_destinatario = array(
                'tipo' => $this->input->post('mtipo'),
                'marca' => $this->input->post('mmarca'),
                'modelo' => $this->input->post('mmodelo'),
                'codigo' => $this->input->post('mcodigo'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('dispositivos',$nuevo_destinatario);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su dispositivo se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'paneldispositivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_dispositivo($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_dispositivo = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('dispositivos',$update_dispositivo,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su dispositivo se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'paneldispositivos');
        redirect(base_url('activospersonales/'));      
    }
    public function agregarInversion_guardar(){
        $nuevo_destinatario = array(
                'tipo' => $this->input->post('mtipo'),
                'monto' => $this->input->post('mmonto'),
                'detalle' => $this->input->post('mdetalle'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('inversiones',$nuevo_destinatario);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su inversión se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_inversion($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_inversion = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('inversiones',$update_inversion,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su inversión se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));      
    }
    public function agregarSeguro_guardar(){
        $nuevo_seguro = array(
                'nombre' => $this->input->post('mseguro'),
                'entidad' => $this->input->post('mentidad'),
                'detalle' => $this->input->post('mdetalle'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('seguros',$nuevo_seguro);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su seguro se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_seguro($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_seguro = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('seguros',$update_seguro,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su seguro se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));      
    }    
    public function agregarCuenta_guardar(){
        $nuevo_cuenta = array(
                'banco' => $this->input->post('mbanco'),
                'tipo' => $this->input->post('mtipo'),
                'numero' => $this->input->post('mnumero'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('cuentas',$nuevo_cuenta);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su cuenta bancaria se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_cuenta($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_cuenta = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('cuentas',$update_cuenta,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su cuenta bancaria se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));      
    }
    
    public function agregarOtro_guardar(){
        $nuevo_otros = array(
                'tipo' => $this->input->post('mtipo'),
                'monto' => $this->input->post('mmonto'),
                'detalle' => $this->input->post('mdetalle'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('otros',$nuevo_otros);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su opción se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_otro($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_otro = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('otros',$update_otro,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su opción se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));      
    }
    public function agregarPasivo_guardar(){
        $nuevo_otros = array(
                'empresa' => $this->input->post('mempresa'),
                'mes' => $this->input->post('mmes'),
                'vencimiento' => $this->input->post('mvencimiento'),
                'monto' => $this->input->post('mmonto'),
                'id_usuario' => $this->session->userdata('m_id')
            );
            $this->MyModel->agregar_model('pasivos',$nuevo_otros);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su pasivo se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelpasivos');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_pasivo($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_pasivo = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('pasivos',$update_pasivo,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su pasivo se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelpasivos');
        redirect(base_url('activospersonales/'));      
    }
    public function agregarRed_guardar(){
        $nuevo_red = array(
                'red' => $this->input->post('mred'),
                'usuario' => $this->input->post('musuario'),
                'email' => $this->input->post('memail'),
                'email_personal' => $this->input->post('memail_personal'),
                'id_usuario' => $this->session->userdata('m_id')
            );
        $guardado = $this->MyModel->agregar_model('redes',$nuevo_red);
            
        $this->session->set_flashdata('msje_creacion', '1');
        $this->session->set_flashdata('msje_text', 'Su red se ha agregado con éxito');
        $this->session->set_flashdata('panel', 'panelredes');
        redirect(base_url('activospersonales/'));
    }
    public function quitar_red($id){
        if(isset($id)){
        $id = $this->general->decrypt($id);    
        $update_red = array(
                    'estado' => '0'
                );
                $this->MyModel->agregar_model('redes',$update_red,'id',$id);
        }
        $this->session->set_flashdata('msje_eliminacion', '1');
        $this->session->set_flashdata('msje_text', 'Su red se ha eliminado con éxito');
        $this->session->set_flashdata('panel', 'panelredes');
        redirect(base_url('activospersonales/'));      
    }

	public function agregar_destinatario_modal(){
        $this->load->view('modal/agregar_destinatario');  
    }
    public function agregar_dispositivo_modal(){
        $this->load->view('modal/agregar_dispositivo');     
    }
    public function agregar_activos_modal(){
        $this->load->view('modal/agregar_inversion');        
    }
    public function agregar_seguro_modal(){
        $this->load->view('modal/agregar_seguro');        
    }
    public function agregar_cuenta_modal(){
        $this->load->view('modal/agregar_cuenta');        
    }
    public function agregar_otros_modal(){
        $this->load->view('modal/agregar_otros');        
    }
    public function agregar_pasivo_modal(){
        $this->load->view('modal/agregar_pasivo');
    }
    public function agregar_red_modal(){
        $this->load->view('modal/agregar_red');
    }
    //EDITAR
    public function editar_destinatario_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('destinatarios');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_destinatario',$data);   
    }
    public function editarDestinatario_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_destinatario = array(                
                'nombres' => $this->input->post('mnombres'),
                'apellidos' => $this->input->post('mapellidos'),
                'email' => $this->input->post('memail'),
                'telefono' => $this->input->post('mtelefono')
                );
                $this->MyModel->agregar_model('destinatarios',$actualiza_destinatario,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su destinatario se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'paneldestinatario');
        redirect(base_url('activospersonales/'));
    }

    public function editar_dispositivo_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('dispositivos');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_dispositivo',$data);   
    }
    public function editarDispositivo_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_destinatario = array(                
                'tipo' => $this->input->post('mtipo'),
                'marca' => $this->input->post('mmarca'),
                'modelo' => $this->input->post('mmodelo'),
                'codigo' => $this->input->post('mcodigo')
                );
        $this->MyModel->agregar_model('dispositivos',$actualiza_destinatario,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su dispositivo se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'paneldispositivos');
        redirect(base_url('activospersonales/'));
    }
    public function editar_inversion_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('inversiones');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_inversion',$data); 
    }
    public function editarInversion_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_inversion = array(                
                'tipo' => $this->input->post('mtipo'),
                'monto' => $this->input->post('mmonto'),
                'detalle' => $this->input->post('mdetalle')
                );
                $this->MyModel->agregar_model('inversiones',$actualiza_inversion,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su inversión se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }

    public function editar_seguro_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('seguros');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_seguro',$data); 
    }
    public function editarSeguro_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_seguro = array(                
                'nombre' => $this->input->post('mnombre'),
                'entidad' => $this->input->post('mentidad'),
                'detalle' => $this->input->post('mdetalle')
                );
                $this->MyModel->agregar_model('seguros',$actualiza_seguro,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su seguro se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function editar_cuenta_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('cuentas');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_cuenta',$data); 
    }
    public function editarCuenta_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_cuenta = array(                
                'banco' => $this->input->post('mbanco'),
                'tipo' => $this->input->post('mtipo'),
                'numero' => $this->input->post('mnumero')
                );
                $this->MyModel->agregar_model('cuentas',$actualiza_cuenta,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su cuenta bancaria se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function editar_otro_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('otros');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_otro',$data); 
    }
    public function editarOtro_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_otro = array(                
                'tipo' => $this->input->post('mtipo'),
                'monto' => $this->input->post('mmonto'),
                'detalle' => $this->input->post('mdetalle')
                );
                $this->MyModel->agregar_model('otros',$actualiza_otro,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su opcion se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelactivos');
        redirect(base_url('activospersonales/'));
    }
    public function editar_pasivo_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('pasivos');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_pasivo',$data); 
    }
    public function editarPasivo_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_pasivo = array(                
                'empresa' => $this->input->post('mempresa'),
                'mes' => $this->input->post('mmes'),
                'vencimiento' => $this->input->post('mvencimiento'),
                'monto' => $this->input->post('mmonto')
                );
                $this->MyModel->agregar_model('pasivos',$actualiza_pasivo,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su pasivo se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelpasivos');
        redirect(base_url('activospersonales/'));
    }

    public function editar_red_modal(){
            $id = $this->input->post('id');
            $this->db->select('*');
            $this->db->from('redes');
            $this->db->where('estado','1');
            $this->db->where('id',$id);     
            $this->db->order_by('id','desc');
            $query = $this->db->get();
            $datos = $query->result_array();    

            $data['datos'] = $datos;
            $this->load->view('modal/editar_red',$data); 
    }
    public function editarRed_guardar(){
        $id_usuario = $this->input->post('mid');
        $actualiza_pasivo = array(                
                'red' => $this->input->post('mred'),
                'usuario' => $this->input->post('musuario'),
                'email' => $this->input->post('memail'),
                'email_personal' => $this->input->post('memail_personal')
                );
                $this->MyModel->agregar_model('redes',$actualiza_pasivo,'id',$id_usuario);            
        $this->session->set_flashdata('msje_creacion', '2');
        $this->session->set_flashdata('msje_text', 'Su red se ha actualizado con éxito');
        $this->session->set_flashdata('panel', 'panelredes');
        redirect(base_url('activospersonales/'));
    }   

}