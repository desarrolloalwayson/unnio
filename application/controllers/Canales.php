<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Canales extends CI_Controller {

	public function __construct(){	
	parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
	$this->load->library('parser');
    //$this->load->model('MyModel');
	$this->init();
	}
	public function init(){
        if(empty($this->session->userdata('m_email'))){
            redirect(base_url('home/login'),'refresh');
            exit();
        }    
    }

	public function contacto(){
		$this->load->view('contacto');
	}
	public function callback(){
		$this->load->view('callback');
	}

	// 

	 public function enviacorreo(){
            $nueva_solicitud = array(            	
                'nombre' => $this->input->post('form-contact-name'),
                'email' => $this->input->post('form-contact-email'),  
                'telefono' => $this->input->post('form-contact-phone'),              
                'hora' => $this->input->post('form-contact-company'),
                'obs' => $this->input->post('form-contact-message')
            );
            // this->load->config('email');
        	// $this->load->library('email');
          
            $body = $this->load->view('../views/correoHtml',$nueva_solicitud,true);            

            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'smtp.gmail.com';
            $config['smtp_port']    = '587';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'contacto@alwayson.cl';
            $config['smtp_pass']    = 'Gt7D$j0u&6d';
            $config['charset']    = 'utf-8';
            // $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not            
        	
            $this->email->initialize($config);        
            $this->email->set_newline("\r\n");  
            $this->email->from('contacto@alwayson.cl');
            $this->email->to('aastorga@alwayson.cl');       
            $this->email->subject('Asistencia Metlife');
            $this->email->message($body);
            // $this->email->message('Se ha creado una nueva solicitud, favor validar.'); 
            $this->email->send();
            $this->email->print_debugger();            
            
            $this->session->set_flashdata('msje_solicitud', 'Hemos recibido su solicitud, nos comunicaremos con usted a la brevedad.');
            redirect(base_url('/canales/callback'));
        }

}