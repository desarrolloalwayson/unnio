<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {

	public function __construct(){	
	parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
    $this->load->library('email');
    $this->load->library('form_validation');
    $this->load->library('javascript');
    $this->load->library('session');
	$this->load->library('parser');
    $this->load->model('MyModel');
	$this->init();	
	}
	public function init(){
        if(empty($this->session->userdata('m_email'))){
            redirect(base_url('home/login'),'refresh');
            exit();
        }    
    }

	public function datos(){
		$id_user = $this->session->userdata('m_id');
		$this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('estado','1');
        $this->db->where('id',$id_user);     
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $datos = $query->result_array();	

        $data['datos'] = $datos;
		$this->load->view('datos',$data);
	}
	public function contrasenha(){

		if($this->input->post('mnewpassword')){

			$id = $this->session->userdata('m_id');
			$email = $this->session->userdata('m_email');
			$password = $this->input->post('mpassword');

			$valido = $this->MyModel->login($email,$password);

			if (!empty($valido)){
				$update_pass = array(
                    'password' => MD5($this->input->post('mnewpassword'))
                );
                $this->MyModel->agregar_model('usuarios',$update_pass,'id',$id);
                $this->session->set_flashdata('msje_contrasenha', '1');
        		redirect(base_url('configuracion/contrasenha/'),'refresh');
        		exit();
        		echo '1';
			}else{
				$this->session->set_flashdata('msje_contrasenha', '0');
        		redirect(base_url('configuracion/contrasenha/'),'refresh');
        		echo '2';
        		exit();
			}

			$this->session->set_flashdata('msje_contrasenha', '0');
        	redirect(base_url('configuracion/contrasenha/'),'refresh');
        	exit();
        	echo '3';
		}else{
			$this->load->view('contrasenha');
		}	
		
	}

	public function logout($mensaje=null){
	    $this->session->unset_userdata('m_email');
	    $this->session->sess_destroy();
	    redirect(base_url('home/login'),'refresh');
  	}
  	public function editar_datos_modal(){
  			$id_usuario = $this->input->post('id_usuario');
			$this->db->select('*');
	        $this->db->from('usuarios');
	        $this->db->where('estado','1');
	        $this->db->where('id',$id_usuario);     
	        $this->db->order_by('id','desc');
	        $query = $this->db->get();
	        $datos = $query->result_array();	

	        $data['datos'] = $datos;
	        $this->load->view('modal/editar_datos',$data);  
    }
    public function editarDatos_guardar(){
    	$id_usuario = $this->input->post('mid');
    	$actualiza_user = array(                
                'nombres' => $this->input->post('mnombres'),
                'apellidos' => $this->input->post('mapellidos'),
                'rut' => $this->input->post('mrut'),
                'email' => $this->input->post('memail'),
                'telefono' => $this->input->post('mtelefono')
            	);
                $this->MyModel->agregar_model('usuarios',$actualiza_user,'id',$id_usuario);
            
        $this->session->set_flashdata('msje_creacion', '1');
        redirect(base_url('configuracion/datos'));
    }
}