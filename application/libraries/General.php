<?php if ( ! defined('BASEPATH')) exit('No esta permitido el acceso'); 
//La primera línea impide el acceso directo a este script
class General {
     public function encrypt($string){ 
        /* Defina la palabra secreta para encriptar */
        $key="w3he+1+!";
        $result = ''; 
        for($i=0; $i<strlen($string); $i++)
            {
                $char = substr($string, $i, 1);
                $keychar = substr($key, ($i % strlen($key))-1, 1);
                $char = chr(ord($char)+ord($keychar));
                $result.=$char;
            }
        $tmpencode=base64_encode($result);
        return str_replace("=", "EQ", str_replace("&", "AND", str_replace("+","PLUS", $tmpencode)));
    }

    public function decrypt($string){ 
        /* La palabra secreta para desencriptar igual que la de encriptar */
        $key="w3he+1+!";
        $result = ''; 
        $string=str_replace("EQ", "=", str_replace("AND", "&", str_replace("PLUS","+", $string)));
        $string = base64_decode($string); 
        for($i=0; $i<strlen($string); $i++) 
            { 
                $char = substr($string, $i, 1); 
                $keychar = substr($key, ($i % strlen($key))-1, 1); 
                $char = chr(ord($char)-ord($keychar)); 
                $result.=$char; 
            } 
        return $result; 
    }
    public function detectar_navegador(){
        $browser=array("IE","OPERA","MOZILLA","NETSCAPE","FIREFOX","SAFARI","CHROME","EDGE");
        $os=array("WIN","MAC","LINUX"); 
        # definimos unos valores por defecto para el navegador y el sistema operativo
        $info['browser'] = "OTHER";
        $info['os'] = "OTHER"; 
        # buscamos el navegador con su sistema operativo
        foreach($browser as $parent){
            $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
            $f = $s + strlen($parent);
            $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
            $version = preg_replace('/[^0-9,.]/','',$version);
            if ($s){
                $info['browser'] = $parent;
                $info['version'] = $version;
            }
        }
        # obtenemos el sistema operativo
        foreach($os as $val)
        {
            if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']),$val)!==false)
                $info['os'] = $val;
        }
        # devolvemos el array de valores
        return $info;
    }

}
?>