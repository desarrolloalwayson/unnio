<?php
class MyModel extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function agregar_model($tabla,$data,$campo_id = null,$id = null) {
        if (!empty($id)) {
            $this->db->where($campo_id, $id);
            $this->db->update($tabla, $data); 
        } else {
            $this->db->insert($tabla, $data);
        }
        return $this->db->insert_id();
    }

    public function buscar_model($tabla,$conditions=null){
        $this->db->from($tabla);
        if (!empty($conditions)) {
            $this->db->where($conditions);
        }  
       
        //$this->db->limit(2000);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function buscar_select($tabla,$id_tabla,$campo,$conditions = null,$id = true){
        if (!empty($conditions)) {
            $this->db->where($conditions);
            //print_r($conditions);
        }
        $this->db->order_by($campo); 
        $query = $this->db->get($tabla);
        $result = array();
        foreach ($query->result_array() as $value) {
            if ($id) {
                    $result[$value[$id_tabla]] = $value[$campo];
                } else {
                     $result[$value[$campo]] = $value[$campo];
                }  
        }
        return $result;
    }
    
    public function select($tabla, $campo,$conditions = null){
        if (!empty($conditions)) {
            $this->db->where(array('id <>' => 1));
        }
        $query = $this->db->get($tabla);
        $result = array();
        foreach ($query->result_array() as $value) {
          $result[$value['id']] = $value[$campo];
        }
        return $result;
    }
    
    public function buscar_permisos(){
        $this->db->from('permisos');
        $this->db->where('es_menu = 1');
        $this->db->order_by('area','orden','ASC');
        $query = $this->db->get();
        $permisos_q = $query->result_array();
        $permisos = array();
        foreach ($permisos_q as $p) {
			$busco_permisos = explode(';',$p['rangos']);
			if (in_array($this->session->userdata['s_id_rango'],$busco_permisos)) {
				if ($p['es_submodulo'] == 1) {
					if (empty($permisos[$p['area']][$p['nombre']])){
						$permisos[$p['area']][$p['nombre']] = array(); 
					}
				} else {
					if ($p['es_subarea'] == 1){
						$permisos[$p['area']][$p['subarea']][] = $p; 
					} else {
						$permisos[$p['area']][] = $p; 
					}
				}
			}

        }
		
        return $permisos;
    }
    
    public function cartera($id_usuario){
        $this->db->select('id_cartera,cartera');
        $this->db->from('carteras');
        $this->db->where("activa ='S'");
        $this->db->where('id_supervisor ='.$id_usuario);
        $this->db->order_by("cartera", "asc");
        $query = $this->db->get();        
        return $query->result_array();
        //return json_encode($result);
    }
    public function listar($tabla,$idtabla,$id,$inner1,$on,$order,$tipo){
        $this->db->from($tabla);
        if(!empty($inner1)){
            $this->db->join($inner1,$on);    
        }
        if(!empty($id) || !empty($idtabla)){
            $this->db->where(array($idtabla => $id));
        }
        $this->db->order_by($order,$tipo);
        $query = $this->db->get();
        $resultado = $query->result_array(); 
        return $resultado;
    }
    public function insert($tabla,$datos){                        
        $this->db->insert($tabla,$datos);
        $ins_solicitud = $this->db->affected_rows();
        if($this->db->affected_rows()>0){
            return true;
        }       
    }
    public function update($id,$idval,$datos,$tabla){
        $this->db->where($id,$idval);
        $this->db->update($tabla, $datos);
        if($this->db->affected_rows()>0){
            return true;
        } 
    }
    public function delete($id,$idval,$tabla){
        $this->db->where($id, $idval);
        $this->db->delete($tabla);
        //$this->db->delete($tabla, array($id => $idval));
    }
    function login($email,$password){
            return $this->db->select('id,nombres,apellidos,rut,email,telefono')
                                 ->from('usuarios')
                                 ->where('email', $email)
                                 ->where('password', MD5($password))
                                 ->where('estado', '1')
                                 ->get()
                                 ->result_array();
    }
    function verificar_rut($rut){
            return $this->db->select('id,nombres,apellidos,rut,email,telefono')
                                 ->from('usuarios')
                                 ->where('rut', $rut)
                                 ->where('estado', '1')
                                 ->get()
                                 ->result_array();
    }
        
         
}
?>