<!DOCTYPE html>
<html lang="es"><!-- class="full-height" -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/login.css');?>" rel="stylesheet">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <style media="screen">
      .modal-dialog.cascading-modal {
        margin-top: 6%;
      }
      .modal-dialog.cascading-modal .modal-header .close {
          margin-right: 0rem;
      }
      .formRegistro .form-control{
        color:  #008fdb;

      }
      .formRegistro .form-control:focus {
        color: #008fdb;
      }
      .errorgm{display: none;color: red;font-size: 12px;font-weight: 500;}
      .sucessgm{display: none;color: #008fda;font-size: 12px;font-weight: 500;}
    </style>

    <script src="<?php echo base_url('assets/js/validarRegistro.js');?>"></script>
    <script src="<?php echo base_url('assets/js/validarIngreso.js');?>"></script>
  </head>
  <body>

      <div id="loader-wrapper">
    			<div id="loader"></div>
    			<div class="loader-section section-left"></div>
          <div class="loader-section section-right"></div>
    	</div>

        <!--Main Navigation-->
        <header class="top-container fixed-top">

          <div class="container">
            <div class="row">
              <div class="col-md-5 col-sm-12 ">

            <div class="logoDos">
              <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="70" widht="70" alt="">
            </div>

              </div>

              <div class="col-md-6 col-sm-12 text-right pt-1 d-none d-md-block">
                <span class="tituloDatos">Atención telefónica</span>
                <br>
                <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2994 1894</span>
              </div>

              <!-- Visible solo para dispositivos móbiles -->
              <div class="col-md-6 col-sm-12 text-center pt-1 d-block d-md-none mt-2">
                <span class="tituloDatos">Atención telefónica: &nbsp;</span>
                <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2994 1894</span>
              </div>
              <!-- Fin Visible solo para dispositivos móbiles -->



            </div>
          </div>
        </header>

            <!--Intro Section-->
            <section class="view intro-2 hm-gradient">



                <div class="full-bg-img">
                    <div class="container flex-center">



                        <div class="d-flex align-items-center content-height">



                            <div class="row flex-center pt-5 mt-5">
                                <div class="col-md-6 text-center text-md-left mt-3 mb-3 marginTop7Rem">
                                    <div class="white-text">
                                        <h1 class="h1-responsive font-weight-bold wow fadeInLeft" data-wow-delay="0.3s">
                                          PROTECCIÓN ACTIVOS PERSONALES
                                        </h1>
                                        <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
                                        <h6 class="wow fadeInLeft" data-wow-delay="0.3s">
                                          Asistencia orientada al respaldo y seguridad de tu información personal de alto valor
                                          familiar, que será enviada a los beneficiarios designados a tu solicitud en caso de
                                          fallecimiento. Además incluye servicio de eliminación y desindexación de tus contenidos en la web.
                                        </h6>
                                        <br>

                                        <button type="button" class="btn btn-default" data-toggle="modal" data-toggle="modal" data-target="#registroModal" onclick = "registroModal();">
                                            Regístrate Aquí
                                        </button>

                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-5 offset-xl-1 mt-3">
                                    <!--Form-->
                                    <div class="card wow fadeInRight" data-wow-delay="0.3s">
                                        <div class="card-body">
                                            <!--Header-->
                                            <div class="text-center">
                                                <h3 class="white-text"><i class="fa fa-user white-text"></i> Ingreso a Plataforma</h3>
                                                <hr class="hr-light">
                                            </div>

                                            <!--Body-->
                                            <!-- <div class="md-form">
                                                <i class="fa fa-user prefix white-text active"></i>
                                                <input type="text" id="form3" class="form-control">
                                                <label for="form3" class="active">Your name</label>
                                            </div> -->
                                          <form method="post" action="<?php echo base_url('home/login');?>" onsubmit="return validarIngreso();">
                                            <div class="md-form">
                                                <i style="margin-top: 0.35rem;" class="fa fa-envelope prefix white-text active"></i>
                                                <input type="email" id="memail" name="memail" class="form-control" required>
                                                <label for="memail" class="active">Ingresa tu Email</label>
                                                <span id="mensajeErrorEmailLogin"></span>
                                            </div>

                                            <div class="md-form">
                                                <i style="margin-top: 0.35rem;" class="fa fa-lock prefix white-text active"></i>
                                                <input type="password" id="mpassword" name="mpassword" class="form-control" required>
                                                <label for="mpassword" class="active">Ingresa tu Contraseña</label>
                                                <span id="mensajeErrorContrasenaLogin"></span>
                                            </div>

                                            <?php 
                                              $msje_login = $this->session->flashdata('msje_login');
                                              if(isset($msje_login) && $msje_login==0){?>
                                            <div class="text-center mt-3" id="errorLogin">
                                              <button id="autoclosable-btn-danger" class="btn btn-danger btn-danger btn-block">
                                                 Usuario y/o contraseña incorrectos
                                              </button>
                                            </div>
                                            <?php }?>

                                            <div class="text-center mt-2">
                                                <button type="submit" id="ingresar" class="btn btn-primary mt-4">Ingresar</button>
                                                <!-- onclick="window.location='home.html'" -->
                                                <hr class="hr-light mb-3 mt-4">

                                                    <div class="inline-ul text-center d-flex justify-content-center">
                                                        <!-- <a class="p-2 m-2 tw-ic"><i class="fa fa-twitter white-text"></i></a>
                                                        <a class="p-2 m-2 tw-ic"><i class="fa fa-linkedin white-text"> </i></a>
                                                        <a class="p-2 m-2 tw-ic"><i class="fa fa-instagram white-text"> </i></a> -->

                                                        <a class="p-2 m-2 tw-ic white-text" href="#">
                                                          ¿Olvidaste tu contraseña? Recupérala aquí
                                                          <!-- <i class="fa fa-phone white-text" aria-hidden="true"></i> -->
                                                          <!-- (+56) 2 2994 1894 -->
                                                        </a>
                                                    </div>
                                            </div>

                                          </form>
                                        </div>
                                    </div>
                                    <!--/.Form-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



    <div class="modal fade" id="registroModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <!--Modal: Contact form-->
        <div class="modal-dialog cascading-modal" role="document">

            <!--Content-->
            <div class="modal-content">

                <!--Header-->
                <div class="modal-header primary-color white-text">
                    <h4 class="title">
                        <!-- <i class="fa fa-pencil"></i> --> Registro de Usuarios</h4>
                    <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php
                $attributes = array('autocomplete' => 'on', 'class' => 'formRegistro');
                echo form_open(base_url('home/registro_guardar'), $attributes);

                //echo form_open(base_url('configuracion/editarDatos_guardar'));?>
                <div class="modal-body" id="RegistroBody"></div>
                  <div class="text-center mt-4 mb-2">
                    <button type="submit" id="registrarme" class="btn btn-primary">Quiero Registrarme
                        <i class="fa fa-send ml-2"></i>
                    </button>
                </div>

                <?php echo form_close();?>
            </div>
            <!--/.Content-->
        </div>
        <!--/Modal: Contact form-->
    </div>

    <div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>Registro de Usuario.</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

                <div class="modal-body">
                    <p>El Usuario ya se encuentra registrado en nuestra Base de Datos</p>                    
                </div> 
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
        </div>
    </div>
</div>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>
    <!-- rut -->
    <script src="<?php echo base_url('assets/js/jquery.rut.js');?>"></script>

    <script type="text/javascript" >
          $(document).ready(function(){
            $('#registrarme').prop('disabled', true);
            $("#mensajeErrorCaracter").css("display", "none");
            setTimeout(function(){ $("#errorLogin").fadeOut(4000);}, 5000);
          	$('body').addClass('loaded');
            <?php 
              $msje_login = $this->session->flashdata('msje_login');
              if(isset($msje_login) && $msje_login==2){?>
                 //$("#mostrarmodal").modal("show");
                 setTimeout(function(){$("#mostrarmodal").modal('show');}, 1000);
            <?php }?>

          });

            function registroModal(id){
              $.ajax({
                    url:"<?php echo base_url('home/registro_modal')?>",
                    type: 'POST',
                    data: {id_usuario:1},
                    success: function(data) {
                    $('#RegistroBody').html(data);
                    },
                    error: function(e) {
                      $('#RegistroBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
                    }
              });
          }
            function comprobarClave(){

                var test = $("#mpassword1").val().length;
                var clave1 = $("#mpassword1").val();
                var clave2 = $("#mpassword2").val();

                if(test<4){
                  $("#mensajeErrorCaracter").css("display", "block");
                  //$("#mensajeErrorContrasena").html('<div class="alert alert-danger">Error: La contraseña debe ser de al menos 4 dígitos.</div>');
                  // document.getElementById("mensajeErrorContrasena").style.display = 'block';
                  // document.getElementById("mensajeErrorContrasena").innerHTML     = "<div class="alert alert-danger">Error: La contraseña debe ser de al menos 4 dígitos.</div>";

                }else{
                  $("#mensajeErrorCaracter").css("display", "none");
                  $('#mensajeErrorContrasena').html('');
                  if (clave1==clave2) 
                    $('#registrarme').prop('disabled', false);
                  else 
                    $('#registrarme').prop('disabled', true);
                }
            } 
        </script>

        <script type="text/javascript" >
          // Animations init
          new WOW().init();
        </script>
    </body>
</html>
