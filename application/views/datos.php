<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <style media="screen" rel="stylesheet">
      .dropdown-toggle::after {
        color: #a4cf4e;
      }
      h4.card-title {
        font-size: 1.2rem;
      }
      .card-title {
        margin-bottom: .4rem;
      }
      .card .card-body .card-text {
        font-size: 1rem;
      }
      .errorgm{display: none;color: red;font-size: 12px;font-weight: 500;}
      .sucessgm{display: none;color: #008fda;font-size: 12px;font-weight: 500;}
    </style>
</head>

<body>
  <!-- Sección Header DATOS PERSONALES -->
  <header class="top-container" >
    <div class="container">
      <div class="row">

        <div class="col-md-5 col-sm-12 ">

          <div class="logoDos">
            <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
          </div>
        </div>

        <div class="col-md-6 text-right pt-1">
          <span class="tituloDatos">Atención telefónica</span>
          <br>
          <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2994 1894</span>
        </div>

        <div class="col-md-1 text-center">
          <ul class="navbar-nav ml-auto nav-flex-icons">
              <li class="nav-item dropdown">

                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                  </div>
              </li>
          </ul>
        </div>

      </div>
    </div>
  </header>

  <!-- Sección Navbar/Menú-->
  <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
        <ul class="navbar-nav mr-auto menuCentrado">
            <li class="nav-item">
              <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Servicios
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#prevencion">Prevención</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#alertaParental">Alerta Parental</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#investigacion">Investigación</a>
                <!--<a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php //echo base_url('home/index');?>#accion">Accion</a>-->
              </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">
                            Chat Online
                    </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Canales
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
            </li>
        </ul>
    </div>
  </nav>

  <section class="container mt-5 mb-5">
    <div class="row justify-content-md-center">

     <div class="col-md-10">
      <?php 
            $msje_creacion = $this->session->flashdata('msje_creacion');
            if(!empty($msje_creacion)){?>
            <div id="alerta_crear" class="alert alert-<?php if($msje_creacion[0]==1){echo 'success';}else{echo 'warning';}?> alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?php if($msje_creacion[0]==1){
                    echo '&Eacute;xito';
                }else{
                    echo 'Atenci&oacute;n';                
                }?></strong> 
            <?php if($msje_creacion[0]==1){
                    echo 'Se ha insertado sin problema';
                  }else{
                    echo 'No se ha podido insertar.';
                }?>
            </div>
            <?php }?>
      </div>


      <div class="col-md-10">

        <div class="card ">
          <div class="card-header primary-color lighten-1 white-text">Datos Personales</div>
          <div class="card-body">
            <div class="row mx-4 mt-2">
              <div class="col-md-6">

                <h4 class="card-title">Nombres: </h4>
                <p class="card-text"><?=$datos[0]['nombres'];?></p>

                  <h4 class="card-title">Rut: </h4>
                  <p class="card-text"><?=$datos[0]['rut'];?></p>

                  <h4 class="card-title">Teléfono: </h4>
                  <p class="card-text"><?=$datos[0]['telefono'];?></p>


              </div>

              <div class="col-md-6">
                <h4 class="card-title">Apellidos: </h4>
                <p class="card-text"><?=$datos[0]['apellidos'];?></p>

                <h4 class="card-title">Email: </h4>
                <p class="card-text"><?=$datos[0]['email'];?></p>
              </div>
            </div>


            <div class="pull-right mt-2">
              <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#editarDatos" onclick = "editarDatos(<?=$datos[0]['id'];?>);"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Perfil</button>
              <a href="<?php echo base_url('home');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>

            </div>

          </div>
        </div>


        <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="editarDatos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>Modificar Perfil</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
             echo form_open(base_url('configuracion/editarDatos_guardar'));?>
            <div class="modal-body" id="editarDatosBody"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                <button type="submit" id="editarDat" class="btn btn-primary">Guardar</button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>




      </div>
    </div>
  </section><!-- Fin SECTION -->






    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>
    <!-- rut -->
    <script src="<?php echo base_url('assets/js/jquery.rut.js');?>"></script>

    <script>
      $(document).ready(function(){
        setTimeout(function(){ $("#alerta_crear").fadeOut(4000);}, 5000);
        $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
          if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });

      });
</script>

<script>
  $(document).ready(function() {
    var toggleAffix = function(affixElement, scrollElement, wrapper) {
    var height = affixElement.outerHeight(),
        top = wrapper.offset().top;

    if (scrollElement.scrollTop() >= top){
        wrapper.height(height);
        affixElement.addClass("affix");
    }
    else {
        affixElement.removeClass("affix");
        wrapper.height('auto');
    }
  };
  $('[data-toggle="affix"]').each(function() {
    var ele = $(this),
        wrapper = $('<div></div>');

    ele.before(wrapper);
    $(window).on('scroll resize', function() {
        toggleAffix(ele, $(this), wrapper);
    });

    // init
    toggleAffix(ele, $(window), wrapper);
  });

  });

  function editarDatos(id_usuario){
    $.ajax({
          url:"<?php echo base_url('configuracion/editar_datos_modal')?>",
          type: 'POST',
          data: {id_usuario:id_usuario},
          success: function(data) {
            $('#editarDatosBody').html(data);
          },
          error: function(e) {
            $('#editarDatosBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
}

</script>

</body>
</html>
