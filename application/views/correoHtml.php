<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Seduc - Educar para servir</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Rajdhani:400,700" rel="stylesheet">
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#82BE33" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; 
						font-family: Arial, sans-serif;">
							AlwaysOn - Seduc
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #7d7d7d; font-family:'Rajdhani',sans-serif; font-size: 18px;">
										<b>Se han contactado mediante el formulario de Asistencia Seduc</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 20px 0; color: #7d7d7d; font-family:'Rajdhani',sans-serif; font-size: 16px; line-height: 20px;">
										
                                        <table style="width: 100%; font-family: 'Rajdhani', sans-serif;">
	                                        <tr style="background-color: #d9d9d9; color: #7d7d7d;">
	                                        	<th width="30%">Campos</th>
	                                        	<th width="70%">Valores</th>
	                                        </tr>

	                                        <tr>
	                                        	<td>Nombre</td>
	                                        	<td><?php echo $nombre;?></td>
	                                        </tr>
	                                        <tr style="background-color: #EBEBEB;">	                                    
	                                        	<td>E-mail</td>
	                                        	<td><?php echo $email;?></td>
	                                        </tr>
	                                        <tr>
	                                        	<td>Teléfono</td>
	                                        	<td><?php echo $telefono;?></td>
	                                        </tr>
	                                        <tr style="background-color: #EBEBEB;">
	                                        	<td>Servicio</td>
	                                        	<td><?php echo $hora;?> </td>
	                                        </tr>
	                                        <tr>
	                                        	<td>Mensaje</td>
	                                        	<td><?php echo $obs;?></td>
	                                        </tr> 

                                        </table>
                                                                                                                        
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#d9d9d9" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #7d7d7d; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										&reg; AlwaysOn 2019
									</td>
									<td align="right" width="25%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
											         &nbsp;		
												</td>
												<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													 &nbsp;
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>