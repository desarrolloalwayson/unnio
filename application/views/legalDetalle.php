<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <style media="screen">
        .dropdown-toggle::after {
            color: #a4cf4e;
        }
        
        .callToAction {
            padding: 20px;
            /* background: #a4cf4e; */
            background: #008fda;
            color: #fff;
            display: flex;
            align-items: center;
        }
        
        .titleSection {
            padding-top: 3rem;
        }
        
        .title {
            left: 33%
        }
    </style>

</head>

<body>
    <div id="loader-wrapper">
        <div id="loader"></div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>

    <!-- Sección Header DATOS PERSONALES -->
    <header class="top-container" id="main">
        <div class="container">
            <div class="row ">

                <div class="col-md-5 col-sm-12 ">

                    <div class="logoUno">
                       <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
                    </div>

                </div>

                <div class="col-md-6 col-sm-12 text-right pt-1 d-none d-md-block">
                    <span class="tituloDatos">Atención telefónica</span>
                    <br>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>

                <!-- Visible solo para dispositivos móbiles -->
                <div class="col-md-6 col-sm-12 text-center pt-1 d-block d-md-none mt-2">
                    <span class="tituloDatos">Atención telefónica: &nbsp;</span>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>
                <!-- Fin Visible solo para dispositivos móbiles -->

                <div class="col-md-1 text-center">
                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                            </a>

                          <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                              <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                              <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                          </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </header>

    <!-- Sección Navbar/Menú-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
            <ul class="navbar-nav mr-auto menuCentrado">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">
                                Home
                            </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Servicios
              </a>
                    <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#prevencion">Prevención</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#alertaParental">Alerta Parental</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#investigacion">Investigación</a>
                    <!--<a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php //echo base_url('home/index');?>#accion">Accion</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php //echo base_url('home/index');?>#legal">Legal Chile</a>-->
                </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">
                            Chat Online
                    </a>
                </li>
                
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Canales
                  </a>
                  <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
                  </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
                </li>
            </ul>

        </div>
    </nav>

    <section class="container mt-5 mb-5">

        <!--Card-->
        <div class="card">
            <div class="card-header primary-color lighten-1 white-text">Detalle Cobertura</div>
            <!--Card content-->
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">                        
                        <!-- <div class="row mt-4 mb-4 justify-content-md-center">
                            <div class="col-md-5">
                                <div class="mb-3">
                                    <span><strong>Nombre del Producto:</strong></span>
                                    <br>
                                    <p class="card-text">Seguro</p>
                                </div>

                                <div class="mb-3">
                                    <span><strong>Rut Cliente asegurado:</strong></span>
                                    <br>
                                    <p class="card-text">16.174.254-k</p>
                                </div>

                                <div class="mb-3">
                                    <span><strong>Prima Mensual:</strong></span>
                                    <br>
                                    <p class="card-text">0.5 UF</p>
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="mb-3">
                                    <span><strong>Cliente Asegurado:</strong></span>
                                    <br>
                                    <p class="card-text">Oscar Alejandro Cornejo Aguila</p>
                                </div>

                                <div class="mb-3">
                                    <span><strong>Inicio Vigencia:</strong></span>
                                    <br>
                                    <p class="card-text">27/09/2018</p>
                                </div>

                            </div>
                        </div> -->

                        <div class="row justify-content-md-center">
                            <div class="col-md-10">

                                <h4 class="h5"><strong>Cobertura legal</strong></h4>
                                <p>
                                    Servicio de asistencia legal que cubre el 100% de honorarios de Abogados de LegalChile en las siguientes prestaciones legales:
                                </p>
                                <hr>
                                <!--Table-->
                                <table class="table table-bordered table-striped table-responsive">
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th>Consultas</th>
                                            <th>Tope</th>
                                            
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <tr>
                                            <th>
                                                <ul>
                                                    <li>Sobre definición y alcance de cyberbullying o acoso a través de plataformas informáticas.</li>
                                                    <li>Sobre conductas constitutivas de cyberbullying.</li>
                                                    <li>Sobre la responsabilidad de los establecimientos educacionales en casos de cyberbullying.</li>
                                                    <li>Sobre la responsabilidad civil de los padres del infractor en casos de acoso o maltrato por medios tecnológicos dentro o fuera de un establecimiento educacional.</li>
                                                    <li>Sobre las posibles consecuencias penales del infractor en casos de cyberbullying.</li>
                                                </ul>
                                            </th>
                                            <th>Ilimitado</th>                                           
                                        </tr>                                       
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Table-->

                                <!--Table-->
                                <table class="table table-bordered table-striped table-responsive">
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th>Asesorías</th>
                                            <th>Tope</th>
                                            
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <tr>
                                            <th>
                                                <ul>
                                                    <li>Redacción de reclamos ante el establecimiento educacional o la superintendencia de educación escolar en casos de cyberbullying.</li>
                                                </ul>
                                            </th>
                                            <th>Ilimitado</th>                                           
                                        </tr>                                       
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Table-->

                                <!--Table-->
                                <table class="table table-bordered table-striped table-responsive">
                                    <!--Table head-->
                                    <thead>
                                        <tr>
                                            <th>Juicios</th>
                                            <th>Tope</th>
                                            
                                        </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                        <tr>
                                            <th>
                                                <ul>
                                                    <li>Representación de la víctima o sus padres en causas civiles en contra del establecimiento educacional o de terceros responsables de acoso tecnológico.</li>
                                                    <li>Representación de la víctima o sus padres en juicios penales por delitos que sean consecuencia de un acoso por medios tecnológicos.</li>
                                                    <li>Defensa de la víctima o sus padres en caso de ser acusados por acoso tecnológico.</li>
                                                    <li>Presentación de recursos de protección en contra del establecimiento de educación por actos ilegales o arbitrarios derivados de una conducta de cyberbullying.</li>
                                                    <li>Demandas por ley anti discriminación, según corresponda.</li>
                                                </ul>
                                            </th>
                                            <th>Ilimitado</th>                                           
                                        </tr>                                       
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Table-->

                                <!-- <p>*En la tabla se puede observar el monto a indemnizar según los meses que el asegurado lleve vigente en la póliza.</p>
                                <br><br>
                                <p style="font-size:12px;">**Este informativo es simplemente un resumen del seguro, el detalle de las coberturas, condiciones y exclusiones del seguro se encuentran en el condicionado general registrado en <a href="www.cmfchile.cl">www.cmfchile.cl</a>                                    bajo los códigos POL 2 2013 0163; POL 3 2014 0335, art. 2 letra L; POL 3 2014 0335, Art.2 letra I. y en las condiciones particulares de la póliza colectiva.</p> -->
                            </div>
                        </div>

                        <div class="pull-right mt-4">
                            <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.location='../home'">
            <i class="fa fa-reply" aria-hidden="true"></i> Volver al Home
          </button>
                        </div>

                    </div>

                </div>
                <!-- Fin ROW SECTION -->
            </div>
        </div>
    </section>


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>

    <script>
        $(document).ready(function() {
            $('body').addClass('loaded');
            //Efecto Scroll
            // $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
            //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            //     var target = $(this.hash);
            //     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            //     if (target.length) {
            //       $('html, body').animate({
            //         scrollTop: target.offset().top
            //       }, 1000);
            //       return false;
            //     }
            //   }
            // });

        });
    </script>

    <script>
        $(document).ready(function() {
            var toggleAffix = function(affixElement, scrollElement, wrapper) {
                var height = affixElement.outerHeight(),
                    top = wrapper.offset().top;

                if (scrollElement.scrollTop() >= top) {
                    wrapper.height(height);
                    affixElement.addClass("affix");
                } else {
                    affixElement.removeClass("affix");
                    wrapper.height('auto');
                }
            };
            $('[data-toggle="affix"]').each(function() {
                var ele = $(this),
                    wrapper = $('<div></div>');

                ele.before(wrapper);
                $(window).on('scroll resize', function() {
                    toggleAffix(ele, $(this), wrapper);
                });

                // init
                toggleAffix(ele, $(window), wrapper);
            });

        });
    </script>


    <script>
        // Select all links with hashes
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    </script>

</body>

</html>