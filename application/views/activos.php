<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> 

    <style media="screen" rel="stylesheet">
    table{
            width:100%;
        }
        .dataTables_filter{
            float:right;
        }
        #tabla_destinatarios_paginate{
            float:right;
        }
        #tabla_dispositivos_paginate{
            float:right;
        }
        #tabla_inversiones_paginate{
            float:right;
        }
        #tabla_seguros_paginate{
            float:right;
        }
        #tabla_cuentas_paginate{
            float:right;
        }
        #tabla_otros_paginate{
            float:right;
        }
        #tabla_pasivos_paginate{
            float:right;
        }
        #tabla_redes_paginate{
            float:right;
        }
        label {
            display: inline-flex;
            margin-bottom: .5rem;
            margin-top: .5rem;
           
        }
      .dropdown-toggle::after {
        color: #a4cf4e;
      }
      h4.card-title {
        font-size: 1.2rem;
      }
      .card-title {
        margin-bottom: .4rem;
      }
      .card .card-body .card-text {
        font-size: 1rem;
      }
      .nav-tabs {
        border: 0;
        padding: .7rem;
        margin-left: 1rem;
        margin-right: 1rem;
        margin-bottom: -22px;
        background-color: #008fda!important;
        z-index: 2;
        position: relative;
        border-radius: 2px;
      }
      .nav-link {
          display: block;
          padding: .5rem 1rem;
      }
      .nav-tabs .nav-link {
        -webkit-transition: all .4s;
        transition: all .4s;
        border: 0;
        color: #fff;
      }
      .nav-tabs .nav-link:hover {
          border-color: #e9ecef #e9ecef #dee2e6;
      }
      
      .nav-tabs .nav-link.active {
          background-color: rgba(255,255,255,.2);
          color: #fff;
          -webkit-transition: all 1s;
          transition: all 1s;
          border-radius: 2px;
      }
      .nav-justified .nav-item {
          -ms-flex-preferred-size: 0;
          flex-basis: 0;
          -webkit-box-flex: 1;
          -ms-flex-positive: 1;
          flex-grow: 1;
          text-align: center;
      }
      .nav-justified .nav-item {
          -ms-flex-preferred-size: 0;
          flex-basis: 0;
          -webkit-box-flex: 1;
          -ms-flex-positive: 1;
          flex-grow: 1;
          text-align: center;
      }
</style>
</head>
<body>
  <header class="top-container" >


<?php 
$panel = $this->session->flashdata('panel');
   if($panel=='paneldispositivos'){
      $tab_dispositivo = 'active';
   }elseif($panel=='panelactivos'){
      $tab_activos = 'active';
   }elseif($panel=='panelpasivos'){
      $tab_pasivos = 'active';
   }elseif($panel=='panelredes'){
      $tab_redes = 'active';
   }else{
      $tab_destinatarios = 'active';
   }

   //echo '<h1>'.$panel.'</h1>';
?>

    <div class="container">
      <div class="row">

        <div class="col-md-5 col-sm-12 ">
          <div class="logoUno">
            <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
          </div>

          <div class="divisorBarraVertical"></div>

          <div class="logoDos">
            <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
          </div>
        </div>

        <div class="col-md-6 text-right pt-1">
          <span class="tituloDatos">Atención telefónica</span>
          <br>
          <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2994 1894</span>
        </div>

        <div class="col-md-1 text-center">
          <ul class="navbar-nav ml-auto nav-flex-icons">
              <li class="nav-item dropdown">

                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                  </div>
              </li>
          </ul>
        </div>

      </div>
    </div>
  </header>

  <!-- Sección Navbar/Menú-->
  <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
        <ul class="navbar-nav mr-auto menuCentrado">
            <li class="nav-item">
              <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Servicios
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#gestorHuellaDigital">Gestor Huella Digital</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#reputacionOnline">Reputación Online</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#asistenciaLegal">Asistencia Legal</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#activosPersonales">Mis activos personales</a>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Chat Online
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                    <a class="dropdown-item waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=44" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">Gestor Huella Digital</a>
                    <a class="dropdown-item waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=44" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">Reputación Online</a>
                    <a class="dropdown-item waves-effect waves-light" href="https://legalchilecl.ladesk.com/scripts/generateWidget.php?v=4.62.13.3&t=1523450992&cwid=281fab45&cwt=onlineform_mobile&vid=i3od3dwy57on9d80b9c4o5qddiowy&ud=%7B%7D&ie=-1&pt=LegalChile%20-%20Chat&ref=https%3A%2F%2Fwww.legalchile.cl%2Fmovil%2Fpaginas.php%3Fid%3Dchat"
                    target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">Asistencia Legal</a>
                    <a class="dropdown-item waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=44" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">Mis activos personales</a>
                </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Canales
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
            </li>
        </ul>
    </div>
  </nav>

  <section class="container mt-5 mb-5">
    <div class="row justify-content-md-center">

    <div class="col-md-12">
      <?php 
            $msje_creacion = $this->session->flashdata('msje_creacion');
            $msje_text = $this->session->flashdata('msje_text');
            if(!empty($msje_creacion)){?>
            <div id="alerta_crear" class="alert alert-<?php if($msje_creacion[0]==1 || $msje_creacion[0]==2){echo 'success';}else{echo 'warning';}?> alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <!--<strong><?php //if($msje_creacion[0]==1){
                    //echo '&Eacute;xito';
                //}elseif($msje_creacion[0]==2){
                  //  echo '&Eacute;xito';
                //}else{
                 //   echo 'Atenci&oacute;n';                
                //}?></strong> -->
            <?php if($msje_creacion[0]==1){
                    echo $msje_text;
                  }elseif($msje_creacion[0]==2){
                    echo $msje_text;
                  }else{
                    echo 'No se ha podido insertar.';
                }?>
            </div>
            <?php }?>
       <?php 
            $msje_eliminacion = $this->session->flashdata('msje_eliminacion');
            $msje_text = $this->session->flashdata('msje_text');
            if(!empty($msje_eliminacion)){?>
            <div id="alerta_crear" class="alert alert-<?php if($msje_eliminacion[0]==1){echo 'success';}else{echo 'warning';}?> alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <!--<strong><?php //if($msje_eliminacion[0]==1){
                    //echo '&Eacute;xito';
                //}else{
                    //echo 'Atenci&oacute;n';                
                //}?></strong>-->
            <?php if($msje_eliminacion[0]==1){
                    echo $msje_text;
                  }else{
                    echo 'No se ha podido eliminar.';
                }?>
            </div>
            <?php }?>     
            
    </div>

      <div class="col-md-12">
        <!-- Nav tabs -->
          <ul class="nav nav-tabs nav-justified">
              <li class="nav-item" data-toggle="tooltip" data-placement="top" title="&#9432; Los destinatarios son las personas que recibirán esta información en caso que fallezca el titular del seguro.">
                <!-- &#8505;  -->
                  <a class="nav-link <?php echo isset($tab_destinatarios)?" active show":"";?>" data-toggle="tab" href="#paneldestinatario" role="tab">Agregar Destinatarios</a>
              </li>

              <li class="nav-item">
                  <a class="nav-link <?php echo isset($tab_dispositivo)?" active show":"";?>" data-toggle="tab" href="#paneldispositivos" role="tab"> Dispositivos</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link <?php echo isset($tab_activos)?" active show":"";?>" data-toggle="tab" href="#panelactivos" role="tab"> Activos</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link <?php echo isset($tab_pasivos)?" active show":"";?>" data-toggle="tab" href="#panelpasivos" role="tab"> Pasivos</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link <?php echo isset($tab_redes)?" active show":"";?>" data-toggle="tab" href="#panelredes" role="tab"> Email/Redes Sociales</a>
              </li>
          </ul>
          <!-- Tab panels -->
          <div class="tab-content card">
              <!--Panel 1-->
              <div class="tab-pane fade <?php echo isset($tab_destinatarios)?"active show":"";?>" id="paneldestinatario" role="tabpanel">
                  <br>
                  <div class="row mx-4 mt-3">

                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="tabla_destinatarios" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($destinatarios as $row) {?>
                                <tr>
                                    <th scope="row"><?=$row['id'];?></th>
                                    <td><?=$row['nombres']?></td>
                                    <td><?=$row['apellidos']?></td>
                                    <td><?=$row['email']?></td>
                                    <td><?=$row['telefono']?></td>
                                    <td>
                                      <a href="#" data-toggle="modal" onclick = "editarDestinatarioModal(<?php echo $row['id']?>);">
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarDestinatarioModal">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                      </a>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_destinatario/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                    </div>

                  </div>

                  <div class="pull-right mt-3 mb-3 mr-3">
                    <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarDestinatariosModal" onclick = "agregarDestinatariosModal('paneldestinatarios');" ><i class="fa fa-pencil" aria-hidden="true"></i> Agregar Destinatarios</button>
                    <a href="<?php echo base_url('activospersonales');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>

                  </div>

              </div>
              <!--/.Panel 1-->

              <!--Panel 2-->
              <div class="tab-pane fade <?php echo isset($tab_dispositivo)?"active show":"";?>" id="paneldispositivos" role="tabpanel">
                  <br>

                  <div class="row mx-4 mt-3">

                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="tabla_dispositivos" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Tipo</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Código</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($dispositivos as $row) {?>
                                <tr>
                                    <th scope="row"><?=$row['id']?></th>
                                    <td><?=$row['tipo']?></td>
                                    <td><?=$row['marca']?></td>
                                    <td><?=$row['modelo']?></td>
                                    <td><?=$row['codigo']?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarDispositivosModal" onclick = "editarDispositivosModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_dispositivo/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                             <?php }?>   


                            </tbody>
                        </table>
                    </div>
                    </div>

                  </div>

                  <div class="pull-right mt-3 mb-3 mr-3">
                    <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarDispositivosModal" onclick="agregarDispositivosModal('paneldispositivos');"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar Dispositivos</button>
                    <a href="<?php echo base_url('activospersonales');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>

                  </div>



              </div>
              <!--/.Panel 2-->

              <!--Panel 3-->
              <div class="tab-pane fade <?php echo isset($tab_activos)?"active show":"";?>" id="panelactivos" role="tabpanel">
                  <br>

                  <div class="row mx-4 mt-3">
                    <div class="col-md-12">

                      <h3><b>Inversiones</b></h3>
                      <hr>

                      <div class="table-responsive">
                        <table id="tabla_inversiones" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Tipo de Inversión</th>
                                    <th>Monto Inicial</th>
                                    <th>Detalle</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php foreach ($inversiones as $row) {?>
                                <tr>
                                    <th scope="row"><?=$row['id']?></th>
                                    <td><?=$row['tipo']?></td>
                                    <td>$ <?=number_format($row['monto'], 0, '', '.');?></td>
                                    <td><?=$row['detalle']?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarInversionModal" onclick = "editarInversionModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_inversion/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>
      
                            </tbody>
                        </table>

                    </div>

                    <div class="pull-right mt-3 mb-3 mr-3">
                      <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarInversionModal" onclick="agregarInversionModal('panelactivos');">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Agregar Inversión
                      </button>
                    </div>


                    <!-- =========================================
                          MODAL ACTIVOS INVERSIONES
                    ============================================== -->
                    <!-- Modal Editar Activos -->
                    <div class="modal fade" id="editarInversionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Inversión</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/editarInversion_guardar'));?>
                                <div class="modal-body" id="editarInversionBody"></div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                  <!-- Modal Agregar Activos -->
                    <div class="modal fade" id="agregarInversionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Inversión</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/agregarInversion_guardar'));?>
                                <div class="modal-body" id="agregarInversionBody"></div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>


                  </div>

                    <div class="col-md-12">
                      <h3><b>Seguros</b></h3>
                      <hr>

                      <div class="table-responsive">
                        <table id="tabla_seguros" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Nombre Seguro</th>
                                    <th>Entidad</th>
                                    <th>Detalle</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($seguros as $row) {?>
                                <tr>
                                    <th scope="row"><?=$row['id']?></th>
                                    <td><?=$row['nombre']?></td>
                                    <td><?=$row['entidad']?></td>
                                    <td><?=$row['detalle']?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarSeguroModal" onclick = "editarSeguroModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_seguro/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>

                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right mt-3 mb-3 mr-3">
                      <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarSegurosModal" onclick="agregarSegurosModal('panelactivos');">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Agregar Seguro</button>
                    </div>

                    <!-- =========================================
                          MODAL ACTIVOS Seguros
                    ============================================== -->
                    <!-- Modal Editar Activos -->
                    <div class="modal fade" id="editarSeguroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Seguro</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/editarSeguro_guardar'));?>
                                <div class="modal-body" id="editarSeguroBody"></div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                  <!-- Modal Agregar Activos -->
                    <div class="modal fade" id="agregarSegurosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Seguro</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/agregarSeguro_guardar'));?>
                                <div class="modal-body" id="agregarSeguroBody"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                  </div>

                    <div class="col-md-12">
                      <h3><b>Cuenta Bancaria</b></h3>
                      <hr>
                      <div class="table-responsive">
                        <table id="tabla_cuentas" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Banco</th>
                                    <th>Tipo de Cuenta</th>
                                    <th>Número de Cuenta</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php foreach ($cuentas as $row) {?>
                                <tr>
       
                                    <th scope="row"><?=$row['id']?></th>
                                    <td><?=$row['banco']?></td>
                                    <td><?=$row['tipo']?></td>
                                    <td><?=$row['numero']?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarCuentaModal" onclick = "editarCuentaModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_cuenta/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>

                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right mt-3 mb-3 mr-3">
                      <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarCuentaModal" onclick="agregarCuentaModal('panelactivos');">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Agregar Cuenta Bancaria
                      </button>
                    </div>

                    <!-- =========================================
                          MODAL ACTIVOS Seguros
                    ============================================== -->
                    <!-- Modal Editar Activos -->
                    <div class="modal fade" id="editarCuentaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Seguro</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/editarCuenta_guardar'));?>
                                <div class="modal-body" id="editarCuentaBody"></div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                  <!-- Modal Agregar Activos -->
                    <div class="modal fade" id="agregarCuentaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Seguro</strong></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php 
                                echo form_open(base_url('activospersonales/agregarCuenta_guardar'));?>
                                <div class="modal-body" id="agregarCuentasBody"></div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                  </div>

                  <div class="col-md-12">
                    <h3><b>Otros</b></h3>
                    <hr>
                    <div class="table-responsive">
                      <table id="tabla_otros" class="table" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Num #</th>
                                  <th>Tipo de Activo</th>
                                  <th>Monto</th>
                                  <th>Detalle</th>
                                  <th>Editar</th>
                                  <th>Eliminar</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php foreach($otros as $row){?>
                              <tr>
                                  <th scope="row"><?=$row['id'];?></th>
                                  <td><?=$row['tipo'];?></td>
                                  <td>$ <?=number_format($row['monto'], 0, '', '.');?></td>
                                  <td><?=$row['detalle'];?></td>
                                  <td>
                                    <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarOtroModal" onclick = "editarOtroModal(<?=$row['id'];?>);">
                                      <i class="fa fa-pencil" aria-hidden="true">
                                      </i>
                                    </button>
                                  </td>

                                  <td>
                                    <a href="#" data-href="<?php echo base_url('activospersonales/quitar_otro/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                  </td>
                              </tr>
                           <?php }?>   
                          </tbody>
                      </table>
                  </div>
                </div>
                  </div>

                  <div class="pull-right mt-3 mb-3 mr-3">
                    <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarOtrosModal" onclick="agregarOtrosModal('panelactivos');"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar Otros</button>
                    <a href="<?php echo base_url('activospersonales');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>
                  </div>

                  <!-- =========================================
                        MODAL ACTIVOS Otros
                  ============================================== -->
                  <!-- Modal Editar Activos -->
                  <div class="modal fade" id="editarOtroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Otros</strong></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <?php 
                              echo form_open(base_url('activospersonales/editarOtro_guardar'));?>
                              <div class="modal-body" id="editarOtroBody"></div>

                              <div class="modal-footer">
                                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                  <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                              <?php echo form_close();?>
                          </div>
                      </div>
                  </div>

                <!-- Modal Agregar Activos -->
                  <div class="modal fade" id="agregarOtrosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Otros</strong></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>

                              <?php 
                              echo form_open(base_url('activospersonales/agregarOtro_guardar'));?>
                              <div class="modal-body" id="agregarOtrosBody"></div>

                              <div class="modal-footer">
                                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                                  <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                              <?php echo form_close();?>


                          </div>
                      </div>
                  </div>

              </div>
              <!--/.Panel 3-->
              <!--Panel 4-->
              <div class="tab-pane fade <?php echo isset($tab_pasivos)?"active show":"";?>" id="panelpasivos" role="tabpanel">
                  <br>
                  <div class="row mx-4 mt-3">
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="tabla_pasivos" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Empresa</th>
                                    <th>Mes Adeudado</th>
                                    <th>Fecha Vencimiento</th>
                                    <th>Monto Adeudado</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach($pasivos as $row){?>
                                <tr>
                                    <th scope="row"><?=$row['id'];?></th>
                                    <td><?=$row['empresa'];?></td>
                                    <td><?=$row['mes'];?></td>
                                    <td><?=$row['vencimiento'];?></td>
                                    <td>$ <?=number_format($row['monto'], 0, '', '.');?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarPasivoModal" onclick = "editarPasivoModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_pasivo/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                  </div>

                  <div class="pull-right mt-3 mb-3 mr-3">
                    <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarPasivosModal" onclick="agregarPasivosModal('panelpasivos');"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar Pasivos</button>
                    <a href="<?php echo base_url('activospersonales');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>
                  </div>
              </div>
              <!--/.Panel 4-->

              <!--Panel 5-->
              <div class="tab-pane fade <?php echo isset($tab_redes)?"active show":"";?>" id="panelredes" role="tabpanel">
                  <br>
                  <div class="row mx-4 mt-3">
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="tabla_redes" class="table" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Num #</th>
                                    <th>Red Social</th>
                                    <th>Usuario Red Social</th>
                                    <th>Email Red Social</th>
                                    <th>Email Personal</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($redes as $row){?>
                                <tr>
                                    <th scope="row"><?=$row['id'];?></th>
                                    <td><?=$row['red'];?></td>
                                    <td><?=$row['usuario'];?></td>
                                    <td><?=$row['email'];?></td>
                                    <td><?=$row['email_personal'];?></td>
                                    <td>
                                      <button type="button" class="btn btn-deep-orange btn-sm" data-toggle="modal" data-target="#editarRedModal" onclick = "editarRedModal(<?=$row['id'];?>);">
                                        <i class="fa fa-pencil" aria-hidden="true">
                                        </i>
                                      </button>
                                    </td>

                                    <td>
                                      <a href="#" data-href="<?php echo base_url('activospersonales/quitar_red/'.$this->general->encrypt($row['id']));?>" data-toggle="modal" data-target="#confirm-delete"><button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true">
                                        </i>
                                      </button></a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                  </div>

                  <div class="pull-right mt-3 mb-3 mr-3">
                    <button type="button" class="btn btn-deep-orange " data-toggle="modal" data-target="#agregarRedesModal" onclick="agregarRedesModal('panelredes');"><i class="fa fa-pencil" aria-hidden="true"></i> Agregar Email/Redes Sociales</button>
                    <a href="<?php echo base_url('activospersonales');?>"><button type="button" class="btn btn-primary"><i class="fa fa-reply" aria-hidden="true"></i> Volver a Home</button></a>
                  </div>
              </div>
              <!--/.Panel 4-->

          </div>

      </div>
    </div>
  </section>

<!-- =========================================
            MODAL DESTINATARIOS
============================================== -->
  <!-- Modal Editar Destinatarios -->
  <div class="modal fade" id="editarDestinatarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Destinatarios</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/editarDestinatario_guardar'));?>
              <div class="modal-body" id="editarDestinatarioBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

<!-- Modal Agregar Destinatarios -->
  <div class="modal fade" id="agregarDestinatariosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Destinatarios</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/agregarDestinatario_guardar'));?>
              <div class="modal-body" id="agregarDestinatarioBody"></div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

  <!-- =========================================
        Modal Dispositivos
  ============================================== -->
  <!-- Modal Editar Dispositivos -->
  <div class="modal fade" id="editarDispositivosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Dispositivos</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/editarDispositivo_guardar'));?>
              <div class="modal-body" id="editarDispositivoBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

<!-- Modal Agregar Dispositivos -->
  <div class="modal fade" id="agregarDispositivosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Dispositivos</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/agregarDispositivo_guardar'));?>
              <div class="modal-body" id="agregarDispositivoBody"></div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>


  <!-- =========================================
        MODAL PASIVOS
  ============================================== -->
  <!-- Modal Editar Pasivos -->
  <div class="modal fade" id="editarPasivoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Pasivos</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/editarPasivo_guardar'));?>
              <div class="modal-body" id="editarPasivoBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

  <!-- Modal Agregar Pasivos -->
  <div class="modal fade" id="agregarPasivosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Pasivos</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/agregarPasivo_guardar'));?>
              <div class="modal-body" id="agregarPasivoBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

  <!-- =========================================
        MODAL EMAIL/REDES SOCIALES
  ============================================== -->
  <!-- Modal Editar Email/Redes Sociales -->
  <div class="modal fade" id="editarRedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Editar Email/Redes Sociales</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/editarRed_guardar'));?>
              <div class="modal-body" id="editarRedBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>

  <!-- Modal Agregar Email/Redes Sociales -->
  <div class="modal fade" id="agregarRedesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><strong>Agregar Email/Redes Sociales</strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <?php 
              echo form_open(base_url('activospersonales/agregarRed_guardar'));?>
              <div class="modal-body" id="agregarRedBody"></div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-deep-orange" data-dismiss="modal">Cerrar</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
              <?php echo form_close();?>
          </div>
      </div>
  </div>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>Confirmaci&oacute;n de Solicitud.</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

                <div class="modal-body">
                    <p>Se eliminara el registro seleccionado</p>
                    <p>&iquest;Desea continuar?</p>
                    
                </div> 
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger btn-ok">Borrar</a>
                </div>
        </div>
    </div>
</div>

        <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>
    <!--desde-->
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap4.min.js');?>"></script>
    
    <!--datatables-eof-->
      <script type="text/javascript">
       $(document).ready(function() {
              $('#tabla_destinatarios,#tabla_dispositivos,#tabla_inversiones,#tabla_seguros,#tabla_cuentas,#tabla_otros,#tabla_pasivos,#tabla_redes').DataTable({
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar\xa0_MENU_\xa0registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:\xa0",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                },
                "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                  "iDisplayLength": 5
                });
         });
    </script>

    <script>
      $(document).ready(function(){
        setTimeout(function(){ $("#alerta_crear").fadeOut(4000);}, 5000);

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
          if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });

      });
</script>

<script>
  $(document).ready(function() {
    var toggleAffix = function(affixElement, scrollElement, wrapper) {
    var height = affixElement.outerHeight(),
        top = wrapper.offset().top;

    if (scrollElement.scrollTop() >= top){
        wrapper.height(height);
        affixElement.addClass("affix");
    }
    else {
        affixElement.removeClass("affix");
        wrapper.height('auto');
    }
  };
  $('[data-toggle="affix"]').each(function() {
    var ele = $(this),
        wrapper = $('<div></div>');

    ele.before(wrapper);
    $(window).on('scroll resize', function() {
        toggleAffix(ele, $(this), wrapper);
    });

    // init
    toggleAffix(ele, $(window), wrapper);
  });

  });

  //confirmacion de archivo
  $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        }); 

  function agregarDestinatariosModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_destinatario_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarDestinatarioBody').html(data);
          },
          error: function(e) {
            $('#agregarDestinatarioBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function agregarDispositivosModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_dispositivo_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarDispositivoBody').html(data);
          },
          error: function(e) {
            $('#agregarDispositivoBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
    function agregarInversionModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_activos_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarInversionBody').html(data);
          },
          error: function(e) {
            $('#agregarInversionBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function agregarSegurosModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_seguro_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarSeguroBody').html(data);
          },
          error: function(e) {
            $('#agregarSeguroBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function agregarCuentaModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_cuenta_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarCuentasBody').html(data);
          },
          error: function(e) {
            $('#agregarCuentasBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function agregarOtrosModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_otros_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarOtrosBody').html(data);
          },
          error: function(e) {
            $('#agregarOtrosBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function agregarPasivosModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_pasivo_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarPasivoBody').html(data);
          },
          error: function(e) {
            $('#agregarPasivoBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
    function agregarRedesModal(elem){
    $.ajax({
          url:"<?php echo base_url('activospersonales/agregar_red_modal')?>",
          type: 'POST',
          data: {id_solicitud:1},
          success: function(data) {  
          $('#agregarRedBody').html(data);
          },
          error: function(e) {
            $('#agregarRedBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function editarDestinatarioModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_destinatario_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarDestinatarioBody').html(data);
          },
          error: function(e) {
            $('#editarDestinatarioBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function editarDispositivosModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_dispositivo_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarDispositivoBody').html(data);
          },
          error: function(e) {
            $('#editarDispositivoBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  function editarInversionModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_inversion_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarInversionBody').html(data);
          },
          error: function(e) {
            $('#editarInversionBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  
  function editarSeguroModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_seguro_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarSeguroBody').html(data);
          },
          error: function(e) {
            $('#editarSeguroBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  
  function editarCuentaModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_cuenta_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarCuentaBody').html(data);
          },
          error: function(e) {
            $('#editarCuentaBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  
  function editarOtroModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_otro_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarOtroBody').html(data);
          },
          error: function(e) {
            $('#editarOtroBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  
  function editarPasivoModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_pasivo_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarPasivoBody').html(data);
          },
          error: function(e) {
            $('#editarPasivoBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }
  
  function editarRedModal(id){
    $.ajax({
          url:"<?php echo base_url('activospersonales/editar_red_modal')?>",
          type: 'POST',
          data: {id:id},
          success: function(data) {
            $('#editarRedBody').html(data);
          },
          error: function(e) {
            $('#editarRedBody').html('<div class="alert alert-danger">Error: NO se puede cargar la vista</div>');
          }
    });
  }


</script>

</body>
</html>
