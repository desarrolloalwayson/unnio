<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <style media="screen">
        .dropdown-toggle::after {
            color: #a4cf4e;
        }
        
        .callToAction {
            padding: 20px;
            /* background: #a4cf4e; */
            background: #008fda;
            color: #fff;
            display: flex;
            align-items: center;
        }
        
        .titleSection {
            padding-top: 3rem;
        }
        
        .title {
            left: 37%;
        }
    </style>

</head>

<body>
    <div id="loader-wrapper">
        <div id="loader"></div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>

    <!-- Sección Header DATOS PERSONALES -->
    <header class="top-container" id="main">
        <div class="container">
            <div class="row ">

                <div class="col-md-5 col-sm-12 ">
                    <div class="logoUno">
                        <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 text-right pt-1 d-none d-md-block">
                    <span class="tituloDatos">Atención telefónica</span>
                    <br>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>

                <!-- Visible solo para dispositivos móbiles -->
                <div class="col-md-6 col-sm-12 text-center pt-1 d-block d-md-none mt-2">
                    <span class="tituloDatos">Atención telefónica: &nbsp;</span>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>
                <!-- Fin Visible solo para dispositivos móbiles -->

                <div class="col-md-1 text-center">
                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                            </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                                  <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                                  <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                              </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </header>

    <!-- Sección Navbar/Menú-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
            <ul class="navbar-nav mr-auto menuCentrado">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">
                                Home
                            </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Servicios
              </a>
                    <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#servicioAsitenciaLegal">Apoyo para tomar medidas legales.</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">
                                Chat Online
                        </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Canales
                    </a>
                  <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
                </li>
            </ul>

        </div>
    </nav>

    <!-- Sección Home/Main -->
    <div class="bgHomeDesempleo">
        <div class="title">
            <p>Conoce tus servicios de </p>
            <h1>Acción</h1>
        </div>

        <div class="botonTitle">
            <!-- <button type="button" class="btn btn-primary">Baja y sorpréndete </button> -->
            <a href="#gestorHuellaDigital" class="animated pulse infinite btn-floating blue waves-effect waves-light smooth-scroll">
                <i class="fa fa-arrow-down"></i>
            </a>
        </div>
    </div>

    <!--DESDE-->
     <section class="container-fluid" id="informeSolicitud">
        <div class="row">
            <div class="col-md-6 imgReputacion d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                    <p class="px-4">
                      <div class="titleSection">  
                       <h2>CONSULTAS</h2>
                      </div>
                      <ul>
                        <li>Sobre definición y alcance de ciberbullyin o acoso a través de plataformas
                        informáticas</li>
                        <li>Sobre conductas constitutivas de ciberbullying</li>
                        <li>Sobre la responsabilidad de los establecimientos educacionales en casos
                        de ciberbullying</li>
                        <li>Sobre la responsabilidad civil de los padres del infractor en casos de acoso
                        o maltrato por medios tecnológicos dentro o fuera de un establecimiento
                        educacional</li>
                        <li>Sobre las posibles consecuencias penales del infractor en casos de
                        ciberbullying</li>
                      </ul>  
                    </p>

                <p class="px-4">
                 <div class="titleSection">
                  <h2>ASESORIAS</h2>
                 </div> 
                  <ul>
                    <li>Redacción de reclamos ante el establecimiento educacional o la superintendencia de educación escolar en casos de ciberbullying</li>
                  </ul>  
                </p>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>

            </div>
            <div class="col-md-6 bgAsistenciaLegal d-block d-md-none"></div>
        </div>
    </section>

    <!-- INICIO REPUTACIÓN ONLINE -->
    <section class="container-fluid" id="newsPeriodicos">
        <div class="row">
            <div class="col-md-6 pt-5 marPadBottom50">

                <p class="px-4">
                 <div class="titleSection">
                  <h2>JUICIOS</h2>
                 </div> 
                  <ul>  
                    <li>Representación de la víctima o sus padres en causas civiles en contra del
                    establecimiento educacional o de terceros responsables de acoso
                    tecnológico</li>
                    <li>Representación de la víctima o sus padres en juicios penales por delitos
                    que sean consecuencia de un acoso por medios tecnológicos.</li>
                    <li>Defensa de la víctima o sus padres en caso de ser acusados por acoso
                    tecnológico</li>
                    <li>Presentación de recursos de protección en contra del establecimiento de
                    educación por actos ilegales o arbitrarios derivados de una conducta de
                    ciberbullying</li>
                    <li>Demandas por ley anti discriminación, según corresponda</li>
                  </ul>

                  <div class="titleSection">
                   <h2>EXCLUSIONES</h2>
                  </div>
                    <b>a)</b> Atención de asuntos ocurridos antes de la entrada en vigencia del contrato.<br>
                    <b>b)</b> Acciones legales en contra de Bancos e Instituciones Financieras, Corredoras y Compañías de
                    Seguro.<br>
                    <b>c)</b> Aquellas materias legales que estén regidas o sometidas a una legislación que no sea la chilena<br>
                    <b>d)</b> Materias no comprendidas en el concepto de ciberbullying o acoso a través de plataformas
                    tecnológicas.<br>
                    <b>e)</b> Aquellos temas que no estén directamente relacionados con el cliente, es decir, que no revistan un
                    interés cierto y efectivo para él o su patrimonio y las materias que no se encuentren en la cobertura.<br>
                    <b>f)</b> Todos los gastos que genere la gestión serán de cargo del cliente. Se entiende por tales el pago de
                    honorarios de notaría, conservadores, archiveros o receptores judiciales; de aranceles de Registro
                    Civil, de Impuestos, y en general cualquier otro gasto que genere la gestión profesional, distintos del
                    honorario de abogados y procuradores.<br>
                    <b>g)</b> La asistencia a audiencias que hayan sido informadas con menos de 48 horas hábiles antes de su
                    celebración<br>
                </p>
                
                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div><br>

            </div>
            <div class="col-md-6 imgHuella"></div>
        </div>
    </section>
    <!--HASTA-->





    <div class="container-fluid">
        <div class="row callToAction">
            <div class="col-md-9 pl-5 mt-2">
                <h3><strong>¿Necesitas asesoría? ¡Conversemos!</strong></h3>
            </div>
            <div class="col-md-3">
                <a class="btn btn-default" href="tel:+56228821715">
                    <i class="fa fa-phone" aria-hidden="true"></i> <strong>(+56) 2 2882 1715</strong>
                </a>
            </div>
        </div>
    </div>


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>

    <script>
        $(document).ready(function() {
            $('body').addClass('loaded');
            //Efecto Scroll
            // $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
            //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            //     var target = $(this.hash);
            //     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            //     if (target.length) {
            //       $('html, body').animate({
            //         scrollTop: target.offset().top
            //       }, 1000);
            //       return false;
            //     }
            //   }
            // });

        });
    </script>

    <script>
        $(document).ready(function() {
            var toggleAffix = function(affixElement, scrollElement, wrapper) {
                var height = affixElement.outerHeight(),
                    top = wrapper.offset().top;

                if (scrollElement.scrollTop() >= top) {
                    wrapper.height(height);
                    affixElement.addClass("affix");
                } else {
                    affixElement.removeClass("affix");
                    wrapper.height('auto');
                }
            };
            $('[data-toggle="affix"]').each(function() {
                var ele = $(this),
                    wrapper = $('<div></div>');

                ele.before(wrapper);
                $(window).on('scroll resize', function() {
                    toggleAffix(ele, $(this), wrapper);
                });

                // init
                toggleAffix(ele, $(window), wrapper);
            });

        });
    </script>


    <script>
        // Select all links with hashes
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    </script>

</body>

</html>