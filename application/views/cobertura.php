<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <style media="screen">
      .dropdown-toggle::after {
        color: #a4cf4e;
      }
      .table-bordered {
          border-right: none;
          border-left: none;
      }

    </style>

</head>

<body>
  <!-- Sección Header DATOS PERSONALES -->
  <header class="top-container" >
    <div class="container">
      <div class="row">

        <div class="col-md-5 col-sm-12 ">

          <div class="logoDos">
            <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
          </div>
          
        </div>

        <div class="col-md-6 text-right pt-1">
          <span class="tituloDatos">Atención telefónica</span>
          <br>
          <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2994 1894</span>
        </div>

        <div class="col-md-1 text-center">
          <ul class="navbar-nav ml-auto nav-flex-icons">
              <li class="nav-item dropdown">

                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                  </div>
              </li>
          </ul>
        </div>

      </div>
    </div>
  </header>

  <!-- Sección Navbar/Menú-->
  <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
        <ul class="navbar-nav mr-auto menuCentrado">
            <li class="nav-item">
              <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Servicios
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#prevencion">Prevención</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#alertaParental">Alerta Parental</a>
                <a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php echo base_url('home/index');?>#investigacion">Investigación</a>
                <!--<a class="dropdown-item waves-effect waves-light smooth-scroll" href="<?php //echo base_url('home/index');?>#accion">Accion</a>-->
              </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">
                            Chat Online
                    </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Canales
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
            </li>
        </ul>

    </div>
  </nav>

  <section class="container mt-5 mb-5">

    <!--Card-->
  <div class="card">
    <div class="card-header primary-color lighten-1 white-text">Detalle Cobertura</div>
    <!--Card content-->
    <div class="card-body">

    <div class="row">
      <div class="col-md-12">

    		<div class="row mt-4 mb-4 justify-content-md-center">

    			<div class="col-md-5">
    				<div class="mb-3">
  			      <span><strong>Nombre del Producto:</strong></span>
              <br>
              <p class="card-text">Seguro</p>
    				</div>

            <div class="mb-3">
  			      <span><strong>Rut Cliente asegurado:</strong></span>
              <br>
  				    <p class="card-text"><?=$datos[0]['rut'];?></p>
    				</div>

            <div class="mb-3">
  			      <span><strong>Prima Mensual:</strong></span>
              <br>
  				    <p class="card-text">0.5 UF</p>
    				</div>

    			</div>

    			 <div class="col-md-5">
    				<div class="mb-3">
        			<span><strong>Cliente Asegurado:</strong></span>
              <br>
    					<p class="card-text"><?=$datos[0]['nombres'].' '.$datos[0]['apellidos'];?></p>
    				</div>

            <div class="mb-3">
        			<span><strong>Inicio Vigencia:</strong></span>
              <br>
    					<p class="card-text">27/09/2018</p>
    				</div>

    			</div>


        </div>

        <div class="row justify-content-md-center">
          <div class="col-md-10">

            <h4 class="h5"><strong>Cobertura / Capital Asegurado</strong></h4>
            <hr>
            <!--Table-->
              <table class="table table-bordered table-striped table-responsive">

                  <!--Table head-->
                  <thead>
                      <tr>
                          <th>&nbsp;</th>
                          <th>Mes 1 al 3</th>
                          <th>Mes 4 al 6</th>
                          <th>Mes 7 al 9</th>
                          <th >Mes 10 al 12</th>
                          <th >Mes 13 al 15</th>
                          <th >Mes 16 al 18</th>
                          <th >Mes 19 en adelante </th>
                      </tr>
                  </thead>
                  <!--Table head-->

                  <!--Table body-->
                  <tbody>
                      <tr>
                          <th>Vida</th>
                          <td>UF 200</td>
                          <td>UF 300</td>
                          <td>UF 400</td>
                          <td>UF 500</td>
                          <td>UF 600</td>
                          <td>UF 700</td>
                          <td>UF 800</td>
                      </tr>
                      <tr>
                        <th>Factura de Huesos <br>(Excluye hueso nasal).</th>
                        <td>UF 2</td>
                        <td>UF 2</td>
                        <td>UF 3</td>
                        <td>UF 3</td>
                        <td>UF 4</td>
                        <td>UF 4</td>
                        <td>UF 5</td>
                      </tr>
                      <tr>
                        <th >Renta Diaria Hospitalaria por Accidente.</th>
                        <td>UF 3</td>
                        <td>UF 3</td>
                        <td>UF 4</td>
                        <td>UF 4</td>
                        <td>UF 5</td>
                        <td>UF 5</td>
                        <td>UF 6</td>
                      </tr>

                  </tbody>
                  <!--Table body-->

              </table>
              <!--Table-->

              <p>*En la tabla se puede observar el monto a indemnizar según los meses que el asegurado lleve vigente en la póliza.</p>
              <br><br>
              <p style="font-size:12px;">**Este informativo es simplemente un resumen del seguro, el detalle de las coberturas, condiciones y exclusiones del
                seguro se encuentran en el condicionado general registrado en <a href="www.cmfchile.cl">www.cmfchile.cl</a> bajo los códigos POL 2 2013 0163; POL 3 2014 0335,
                art. 2 letra L; POL 3 2014 0335, Art.2 letra I. y en las condiciones particulares de la póliza colectiva.</p>
          </div>
        </div>

        <div class="pull-right mt-4">
          <button type="button" class="btn btn-primary" onclick="window.location='home.html'">
            <i class="fa fa-reply" aria-hidden="true" ></i> Volver al Home
          </button>
        </div>

  	</div>

  </div><!-- Fin ROW SECTION -->
  </div>
</div>
</section><!-- Fin SECTION -->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>

    <script>
      $(document).ready(function(){
        $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
          if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });

      });
</script>

<script>
  $(document).ready(function() {
    var toggleAffix = function(affixElement, scrollElement, wrapper) {
    var height = affixElement.outerHeight(),
        top = wrapper.offset().top;

    if (scrollElement.scrollTop() >= top){
        wrapper.height(height);
        affixElement.addClass("affix");
    }
    else {
        affixElement.removeClass("affix");
        wrapper.height('auto');
    }
  };
  $('[data-toggle="affix"]').each(function() {
    var ele = $(this),
        wrapper = $('<div></div>');

    ele.before(wrapper);
    $(window).on('scroll resize', function() {
        toggleAffix(ele, $(this), wrapper);
    });

    // init
    toggleAffix(ele, $(window), wrapper);
  });

  });
</script>

</body>
</html>
