<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <style media="screen">
        .dropdown-toggle::after {
          color: #a4cf4e;
        }
      .callToAction{
        padding: 20px;
        /* background: #a4cf4e; */
        background: #008fda;
        color: #fff;
        display: flex;
        align-items: center;
      }
      .titleSection{padding-top: 3rem;}
    </style>

</head>

<body>
  <div id="loader-wrapper">
            <div id="loader"></div>

            <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>

        </div>

  <!-- Sección Header DATOS PERSONALES -->
  <header class="top-container" id="main">
    <div class="container">
      <div class="row ">
        <div class="col-md-5 col-sm-12 ">

          <div class="logoDos">
            <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
          </div>
          
        </div>

        <div class="col-md-6 col-sm-12 text-right pt-1 d-none d-md-block">
          <span class="tituloDatos">Atención telefónica</span>
          <br>
          <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
        </div>

        <!-- Visible solo para dispositivos móbiles -->
        <div class="col-md-6 col-sm-12 text-center pt-1 d-block d-md-none mt-2">
          <span class="tituloDatos">Atención telefónica: &nbsp;</span>
          <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
        </div>
        <!-- Fin Visible solo para dispositivos móbiles -->

        <div class="col-md-1 text-center">
          <ul class="navbar-nav ml-auto nav-flex-icons">
              <li class="nav-item dropdown">

                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                  </div>
              </li>
          </ul>
        </div>

      </div>
    </div>
  </header>

  <!-- Sección Navbar/Menú-->
  <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
        <ul class="navbar-nav mr-auto menuCentrado">
            <li class="nav-item">
              <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Servicios
                </a>
                <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#prevencion">Prevención</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#alertaParental">Alerta Parental</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#investigacion">Investigación</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#guiaUso">Guía de Uso</a>
                    <!--<a class="dropdown-item waves-effect waves-light smooth-scroll" href="#accion">Accion</a>
                    <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#legal">Legal Chile</a>-->
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link waves-effect waves-light" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" href="https://lc.chat/now/9928370/9">
                    Chat Online
                </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Canales
              </a>
              <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
              </div>
            </li>

            <?php
            /*
            <li class="nav-item">
              <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
            </li>
            */
            ?>
        </ul>

    </div>
  </nav>

  <!-- Sección Home/Main -->
    <div class="bgHome">
        <div class="title">
            <p>Descubre tus</p>
            <h1>Servicios de <br>Asistencia</h1>
            <!-- <h2>Always On</h2> -->
        </div>

        <div class="botonTitle">
            <!-- <button type="button" class="btn btn-primary">Baja y sorpréndete </button> -->
            <a href="#gestorHuellaDigital" class="animated pulse infinite btn-floating blue waves-effect waves-light smooth-scroll">
                <i class="fa fa-arrow-down"></i>
            </a>
        </div>
    </div>

    <!-- Sección Asistencia Hogar -->
    <section class="container-fluid" id="prevencion">
        <div class="row">
            <div class="col-md-6 imgLeft d-none d-md-block"></div>
            <div class="col-md-6 pt-5 pb-4 marPadBottom50">
                <div class="titleSection">
                    <h2>Prevención</h2>
                </div>
                <br>
                <p class="px-4">
                    <!-- Asesoría en el control y monitoreo de la comunicación de tu hijo en la web y en sus dispositivos.</p> -->
                    Asesoría para el control y monitoreo de la comunicación del niño en la web.
                <br>

                <ul>
                    <li class="card-text">Control Parental.</li>
                    <li class="card-text">Control y bloqueo de Contactos.</li>
                    <li class="card-text">Bloqueo de contactos en WhatsApp (reactivo).</li>
                    <li class="card-text">Control de privacidad.</li>
                    <li class="card-text">Geolocalización y seguridad periférica.</li>
                </ul>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <!-- <a href="<?php echo base_url('servicios/cobertura');?>" class="btn btn-primary waves-effect text-center  ">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Ver Servicio
                    </a> -->
                    <a href="https://secure.livechatinc.com/licence/9928370/v2/open_chat.cgi?groups=4" class="btn btn-primary waves-effect text-center" target="_blank">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Solicitar chat
                    </a>
                </div>

            </div>
            <!-- Visible solo para dispositivos móbiles -->
            <div class="col-md-6 imgLeft d-block d-md-none"></div>
            <!-- Fin Visible solo para dispositivos móbiles -->
        </div>
    </section>



    <!-- INICIO Asistencia Vida -->
    <section class="container-fluid" id="alertaParental">
        <div class="row">
            <div class="col-md-6 bgAsistenciaLegal d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Alerta Parental</h2>
                    <!-- <h4></h4> -->
                </div>
                <br>
                    <p class="px-4"><!-- Identificación y notificación de nuevos riesgos asociadas a uso de internet por parte de menores. -->
                      Notificación de nuevos riesgos asociadas a uso de internet por parte de menores                      
                    </p>
                <br>

                <ul>
                    <li class="card-text">News periódicos.</li>
                    <li class="card-text">Alertas de Emergencia.</li>
                </ul>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <!-- <a href="<?php echo base_url('servicios/alertaParental');?>" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Ver Servicio
                    </a> -->
                    <a href="https://secure.livechatinc.com/licence/9928370/v2/open_chat.cgi?groups=4" class="btn btn-primary waves-effect text-center" target="_blank">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Solicitar chat
                    </a>
                </div>

            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row callToAction">
            <div class="col-md-9 pl-5 mt-2">
                <h3><strong>¿Necesitas asesoría? ¡Conversemos!</strong></h3>
            </div>
            <div class="col-md-3">
                <a class="btn btn-deep-orange" href="tel:+56229941894">
                    <i class="fa fa-phone" aria-hidden="true"></i> <strong>(+56) 2 2882 1715</strong>
                </a>
            </div>
        </div>
    </div>

    <!-- INICIO Asistencia Ciberbullying -->
    <section class="container-fluid" id="investigacion">
        <div class="row">
            <div class="col-md-6 bgReputacionOnline d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Investigación</h2>
                    <!-- <h4></h4> -->
                </div>
                <br>
                <p class="px-4">Identificación y análisis del problema </p>
                <br>

                <ul>
                    <li class="card-text">Peritaje informático (certificado).</li>
                </ul>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <!-- <a href="<?php echo base_url('servicios/investigacion');?>" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Ver Servicio
                    </a> -->
                    <a href="https://secure.livechatinc.com/licence/9928370/v2/open_chat.cgi?groups=4" class="btn btn-primary waves-effect text-center" target="_blank">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Solicitar chat
                    </a>
                </div>

            </div>
            <div class="col-md-6 bgAsistenciaLegal d-block d-md-none"></div>
        </div>
    </section>

    <!--GUIA DE USO FIN INI-->
        <section class="container-fluid" id="guiaUso">
        <div class="row">
            <div class="col-md-6 pt-3 marPadBottom50">
                <div class="titleSection">
                    <h2>Guía de Uso</h2>
                </div>
                    <p class="px-4">
                      Se puede hacer uso de las herramientas antes mencionadas en los siguientes dispositivos:
                    </p>
                <p class="px-4"><b>Padres</b>:</p>
                <ul>
                    <li class="card-text">Smartphones y tablets con sistema operativo Android versión 4.4 (KitKat) o superior.</li>
                    <li class="card-text">Smartphones (iPhone) y tablets (iPad) con sistema operativo iOS versión 9.0 o superior.</li>
                </ul>

                <p class="px-4"><b>Hijos</b>:</p>

                <ul>
                    <li class="card-text">
                        Smartphones y tablets con sistema operativo Android versión 7.0 o superior (versiones 5.0 o 6.0 dependerán del dispositivo).</li>
                    <li>
                        Smartphones (iPhone) y tablets (iPad) con sistema operativo iOS: no hay restricción de versión, per sí de funcionalidades.</li>

                <p>
                    Las herramientas tienen mayor grado de incidencia en dispositivos de niños menores de 13 años y pueden utilizarse en grupos familiares de hasta 6 personas.<br>
                    Todas las herramientas pueden variar su grado de incidencia dependiendo del dispositivo móvil, cuenta de Google, entre otros factores, por lo que se invita al asegurado a comunicarse con uno de nuestros ejecutivos a través del Chat Online o del teléfono (+56) 2 2882 1715 para saber más detalles."
                </p>    

            </div>
            <div class="col-md-6 bgguiaUso"></div>
        </div>
    </section>
    <!--GUIA DE USO FIN-->

    <!-- INICIO Asistencia Desempleo -->
    <!--<section class="container-fluid" id="accion">
        <div class="row">

            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Acción</h2>
                </div>
                <br>
                <p class="px-4 mb-5">Apoyo para tomar medidas legales.</p>

                <br>
                <ul>
                  <li>
                    Servicio de asistencia legal para los padres del niño(a) que sea víctima de Ciberbullying, hostigamientos o Acoso Sexual a través de las RRSS o internet.

                  </li>
                </ul>
                <br>

                <div class="text-center mt-5">
                    <a class="btn btn-primary text-center" href="https://secure.livechatinc.com/licence/9928370/v2/open_chat.cgi?groups=4" target="_blank">
                        <i class="fa fa-eye" aria-hidden="true"></i> Solicitar chat
                    </a>
                </div>
            </div>
            <div class="col-md-6 bgActivosPersonales"></div>
        </div>
    </section>-->


    <!-- LEGAL -->
    <!--
    <section class="container-fluid" id="legal">
        <div class="row">
            <div class="col-md-6 bgLegal d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Legal chile</h2>
                </div>
                <br>
                <p class="px-4">Informa sobre consecuencias penales, responsabilidades, demandas.</p>
                <br>

                <ul>
                    <li class="card-text">Representación de la victima o padres.</li>
                    <li class="card-text">Prestación de recursos de protección, entre otras cosas.</li>                    
                </ul>
                <br>
    
                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="<?php //echo base_url('servicios/cobertura');?>" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-eye" aria-hidden="true"></i> &nbsp; Ver más
                    </a>
                </div>

            </div>
            <div class="col-md-6 bgLegal d-block d-md-none"></div>
        </div>
    </section>-->


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>

    <script>
      $(document).ready(function(){
        $('body').addClass('loaded');
        //Efecto Scroll
        // $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
        //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        //     var target = $(this.hash);
        //     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        //     if (target.length) {
        //       $('html, body').animate({
        //         scrollTop: target.offset().top
        //       }, 1000);
        //       return false;
        //     }
        //   }
        // });

      });
</script>

<script>
  $(document).ready(function() {
    var toggleAffix = function(affixElement, scrollElement, wrapper) {
    var height = affixElement.outerHeight(),
        top = wrapper.offset().top;

    if (scrollElement.scrollTop() >= top){
        wrapper.height(height);
        affixElement.addClass("affix");
    }
    else {
        affixElement.removeClass("affix");
        wrapper.height('auto');
    }
  };
  $('[data-toggle="affix"]').each(function() {
    var ele = $(this),
        wrapper = $('<div></div>');

    ele.before(wrapper);
    $(window).on('scroll resize', function() {
        toggleAffix(ele, $(this), wrapper);
    });

    // init
    toggleAffix(ele, $(window), wrapper);
  });

  });
</script>


<script>
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


</script>

</body>
</html>
