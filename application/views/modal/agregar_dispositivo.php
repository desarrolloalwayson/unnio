<div class="md-form">
    <i class="fa fa-laptop prefix grey-text"></i>
    <input type="text" id="mtipo" name="mtipo" class="form-control" required>
    <input type="hidden" name="panel" id="panel" value="paneldispositivos" readonly="readonly">    
    <label for="mtipo">Tipo</label>
</div>

<div class="md-form">
    <i class="fa fa-copyright prefix grey-text"></i>
    <input type="text" id="mmarca" name="mmarca" class="form-control" required>
    <label for="mmarca">Marca</label>
</div>

<div class="md-form">
    <i class="fa fa-info-circle prefix grey-text"></i>
    <input type="text" id="mmodelo" name="mmodelo" class="form-control" required>
    <label for="mmodelo">Modelo</label>
</div>

<div class="md-form">
    <i class="fa fa-barcode prefix grey-text"></i>
    <input type="text" id="mcodigo" name="mcodigo" class="form-control" required>
    <label for="mcodigo">Código</label>
</div>