    <div class="md-form">
        <i class="fa fa-laptop prefix grey-text active"></i>
        <input type="text" id="mtipo" name="mtipo" value="<?=$datos[0]['tipo'];?>" class="form-control" required>
        <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
        <label for="mtipo active" class="active">Tipo Dispositivo</label>
    </div>
    <div class="md-form">
        <i class="fa fa-copyright prefix grey-text active"></i>
        <input type="text" id="mmarca" name="mmarca" value="<?=$datos[0]['marca'];?>" class="form-control" required>
        <label for="mmarca active" class="active">Marca</label>
    </div>
    <div class="md-form">
        <i class="fa fa-info-circle prefix grey-text active"></i>
        <input type="text" id="mmodelo" name="mmodelo" value="<?=$datos[0]['modelo'];?>" class="form-control" required>
        <label for="mmodelo active" class="active">Modelo</label>
    </div>
    <div class="md-form">
        <i class="fa fa-barcode prefix grey-text active"></i>
        <input type="text" id="mcodigo" name="mcodigo" value="<?=$datos[0]['codigo'];?>" class="form-control" required>
        <label for="mcodigo active" class="active">Código</label>
    </div>
