    <div class="md-form">
        <i class="fa fa-building prefix grey-text active"></i>
        <input type="text" id="mempresa" name="mempresa" value="<?=$datos[0]['empresa'];?>" class="form-control" required>
        <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
        <label for="mempresa active" class="active">Empresa</label>
    </div>

    <div class="md-form">
        <i class="fa fa-calendar prefix grey-text active"></i>
        <input type="text" id="mmes" name="mmes" value="<?=$datos[0]['mes'];?>" class="form-control" required>
        <label for="mmes active" class="active">Mes Adeudado</label>
    </div>

    <div class="md-form">
        <i class="fa fa-calendar prefix grey-text active"></i>
        <input type="text" id="mvencimiento" name="mvencimiento" value="<?=$datos[0]['vencimiento'];?>" class="form-control" required>
        <label for="mvencimiento active" class="active">Fecha Vencimiento</label>
    </div>

    <div class="md-form">
        <i class="fa fa-usd prefix grey-text active"></i>
        <input type="number" id="mmonto" name="mmonto" value="<?=$datos[0]['monto'];?>" class="form-control" required>
        <label for="mmonto active" class="active">Monto Adeudado</label>
    </div>