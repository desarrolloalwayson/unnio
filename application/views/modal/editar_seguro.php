  <div class="md-form">
      <i class="fa fa-heart prefix grey-text active"></i>
      <input type="text" id="mnombre" name="mnombre" value="<?=$datos[0]['nombre'];?>" class="form-control" required>
      <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
      <label for="mnombre active" class="active">Nombre Seguro</label>
  </div>

  <div class="md-form">
      <i class="fa fa-flag prefix grey-text active"></i>
      <input type="text" id="mentidad" name="mentidad" value="<?=$datos[0]['entidad'];?>" class="form-control" required>
      <label for="mentidad active" class="active">Entidad</label>
  </div>

  <div class="md-form">
      <i class="fa fa-file-text-o prefix grey-text active"></i>
      <input type="text" id="mdetalle" name="mdetalle" value="<?=$datos[0]['detalle'];?>" class="form-control" required>
      <label for="mdetalle active" class="active">Detalle</label>
  </div>