  <div class="md-form">
      <i class="fa fa-user prefix grey-text active"></i>
      <input type="text" id="mnombres" name="mnombres" value="<?=$datos[0]['nombres'];?>" class="form-control">
      <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
      <label for="materialFormContactNameEx active" class="active">Nombres</label>
  </div>

  <div class="md-form">
      <i class="fa fa-user prefix grey-text active"></i>
      <input type="text" id="mapellidos" name="mapellidos" value="<?=$datos[0]['apellidos'];?>" class="form-control">
      <label for="materialFormContactLastNameEx active" class="active">Apellidos</label>
  </div>

  <div class="md-form">
      <i class="fa fa-id-card-o prefix grey-text active"></i>
      <input type="text" id="mrut" name="mrut"  value="<?=$datos[0]['rut'];?>" autocomplete="off" class="form-control" maxlength="12">
      <label for="materialFormContactRutEx active" class="active">RUT</label>
      <p id="mensajeSucessRut" class="sucessgm">El Rut es Valido.</p>
      <p id="mensajeErrorRut" class="errorgm">El Rut es Invalido.</p>
  </div>

  <div class="alert alert-danger alert-dismissible" id="alerta_rut" style="display: none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Atenci&oacute;n!</strong> El Rut es Incorrecto. Favor validar.
        </div>


  <div class="md-form">
      <i class="fa fa-envelope prefix grey-text active"></i>
      <input type="email" id="memail" name="memail" value="<?=$datos[0]['email'];?>" class="form-control">
      <label for="materialFormContactEmailEx active" class="active">Email</label>
  </div>

  <div class="md-form">
      <i class="fa fa fa-phone prefix grey-text active"></i>
      <input type="tel" id="mtelefono" name="mtelefono" value="<?=$datos[0]['telefono'];?>" class="form-control">
      <label for="materialFormContactTelEx active" class="active">Teléfono</label>
  </div>

    <script type="text/javascript">
      $(function(){
        $("#mrut")
            .rut({formatOn: 'keyup', validateOn: 'keyup'})
            .on('rutInvalido', function(){
                //$(this).parents(".control-group").addClass("error")
                //alert('2');
                $("#mensajeSucessRut").css("display", "none");
                $("#mensajeErrorRut").css("display", "block");
                $('#registrarme').prop('disabled', true);
            })
            .on('rutValido', function(){
                //$(this).parents(".control-group").removeClass("error")
                //alert('1');
                $("#mensajeSucessRut").css("display", "block");
                $("#mensajeErrorRut").css("display", "none");
            });        
      });
    </script>