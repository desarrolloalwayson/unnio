    <div class="md-form">
        <i class="fa fa-handshake-o prefix grey-text"></i>
        <input type="text" id="mtipo" name="mtipo" value="<?=$datos[0]['tipo'];?>" class="form-control" required>
        <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
        <label for="mtipo active" class="active">Tipo de Activo</label>
    </div>

    <div class="md-form">
        <i class="fa fa-usd prefix grey-text"></i>
        <input type="number" id="mmonto" name="mmonto" value="<?=$datos[0]['monto'];?>" class="form-control" required>
        <label for="mmonto active" class="active">Monto</label>
    </div>

    <div class="md-form">
        <i class="fa fa-file-text-o prefix grey-text"></i>
        <input type="text" id="mdetalle" name="mdetalle" value="<?=$datos[0]['detalle'];?>" class="form-control" required>
        <label for="mdetalle active" class="active">Detalle</label>
    </div>