  <div class="md-form">
      <i class="fa fa-heart prefix grey-text"></i>
      <input type="text" id="mseguro" name="mseguro" class="form-control" required>
      <label for="mseguro">Nombre Seguro</label>
  </div>

  <div class="md-form">
      <i class="fa fa-flag prefix grey-text"></i>
      <input type="text" id="mentidad" name="mentidad" class="form-control" required>
      <label for="mentidad">Entidad</label>
  </div>

  <!-- Material input email -->
  <div class="md-form">
      <i class="fa fa-file-text-o prefix grey-text"></i>
      <input type="text" id="mdetalle" name="mdetalle" class="form-control" required>
      <label for="mdetalle">Detalle</label>
  </div>