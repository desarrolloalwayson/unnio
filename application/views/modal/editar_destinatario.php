<div class="md-form">
    <i class="fa fa-user prefix grey-text active"></i>
    <input type="text" id="mnombres" name="mnombres" value="<?=$datos[0]['nombres'];?>" class="form-control" required>
    <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
    <label for="mnombres active" class="active">Nombres</label>
</div>
<div class="md-form">
    <i class="fa fa-user prefix grey-text active"></i>
    <input type="text" id="mapellidos" name="mapellidos" value="<?=$datos[0]['apellidos'];?>" class="form-control" required>
    <label for="mapellidos active" class="active">Apellidos</label>
</div>

<div class="md-form">
    <i class="fa fa-envelope prefix grey-text active"></i>
    <input type="email" id="memail" name="memail" value="<?=$datos[0]['email'];?>" class="form-control" required>
    <label for="memail active" class="active">Email</label>
</div>

<div class="md-form">
    <i class="fa fa-phone prefix grey-text active"></i>
    <input type="tel" id="mtelefono" name="mtelefono" value="<?=$datos[0]['telefono'];?>" class="form-control" required>
    <label for="mtelefono active" class="active">Teléfono</label>
</div>