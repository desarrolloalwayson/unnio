    <div class="md-form">
        <i class="fa fa-hand-o-right prefix grey-text active"></i>
        <input type="text" id="mred" name="mred" value="<?=$datos[0]['red'];?>" class="form-control" required>
        <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
        <label for="mred active" class="active">Red Social</label>
    </div>

    <div class="md-form">
        <i class="fa fa-user prefix grey-text active"></i>
        <input type="text" id="musuario" name="musuario" value="<?=$datos[0]['usuario'];?>" class="form-control" required>
        <label for="musuario active" class="active">Usuario Red Social</label>
    </div>
    <div class="md-form">
        <i class="fa fa-envelope prefix grey-text active"></i>
        <input type="email" id="memail" name="memail" value="<?=$datos[0]['email'];?>" class="form-control" required>
        <label for="memail active" class="active">Email Red Social</label>
    </div>
    <div class="md-form">
        <i class="fa fa-envelope prefix grey-text active"></i>
        <input type="email" id="memail_personal" name="memail_personal" value="<?=$datos[0]['email_personal'];?>" class="form-control" required>
        <label for="memail_personal active" class="active">Email Personal</label>
    </div>