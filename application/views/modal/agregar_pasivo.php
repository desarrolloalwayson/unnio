<div class="md-form">
    <i class="fa fa-building prefix grey-text"></i>
    <input type="text" id="mempresa" name="mempresa" class="form-control" required>
    <label for="mempresa">Empresa</label>
</div>

<div class="md-form">
    <i class="fa fa-calendar prefix grey-text"></i>
    <input type="text" id="mmes" name="mmes" class="form-control" required>
    <label for="mmes">Mes Adeudado</label>
</div>
<div class="md-form">
    <i class="fa fa-calendar prefix grey-text"></i>
    <input type="text" id="mvencimiento" name="mvencimiento" class="form-control" required>
    <label for="mvencimiento">Fecha Vencimiento</label>
</div>
<div class="md-form">
    <i class="fa fa-usd prefix grey-text"></i>
    <input type="number" id="mmonto" name="mmonto" class="form-control" required>
    <label for="mmonto">Monto Adeudado</label>
</div>