  <div class="md-form">
      <i class="fa fa-university prefix grey-text"></i>
      <input type="text" id="mbanco" name="mbanco" class="form-control" required>
      <label for="mbanco">Banco</label>
  </div>

  <div class="md-form">
      <i class="fa fa-id-card-o prefix grey-text"></i>
      <input type="text" id="mtipo" name="mtipo" class="form-control" required>
      <label for="mtipo">Tipo de Cuenta</label>
  </div>

  <div class="md-form">
      <i class="fa fa-credit-card prefix grey-text"></i>
      <input type="text" id="mnumero" name="mnumero" class="form-control" required>
      <label for="mnumero">Número de Cuenta</label>
  </div>
