<div class="md-form">
    <i class="fa fa-usd prefix grey-text"></i>
    <input type="text" id="mtipo" name="mtipo" class="form-control" required>
    <label for="mtipo">Tipo Inversión</label>
</div>

<div class="md-form">
    <i class="fa fa-money prefix grey-text"></i>
    <input type="number" id="mmonto" name="mmonto" class="form-control" required>
    <label for="mmonto">Monto Inicial</label>
</div>

<!-- Material input email -->
<div class="md-form">
    <i class="fa fa-file-text-o prefix grey-text"></i>
    <input type="text" id="mdetalle" name="mdetalle" class="form-control" required>
    <label for="mdetalle">Detalle Inversión</label>
</div>