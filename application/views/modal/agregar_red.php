    <div class="md-form">
        <i class="fa fa-hand-o-right prefix grey-text"></i>
        <input type="text" id="mred" name="mred" class="form-control" required>
        <label for="mred">Red Social</label>
    </div>

    <div class="md-form">
        <i class="fa fa-user prefix grey-text"></i>
        <input type="text" id="musuario" name="musuario" class="form-control" required>
        <label for="musuario">Usuario Red Social</label>
    </div>
    <div class="md-form">
        <i class="fa fa-envelope prefix grey-text"></i>
        <input type="email" id="memail" name="memail" class="form-control" required>
        <label for="memail">Email Red Social</label>
    </div>
    <div class="md-form">
        <i class="fa fa-envelope prefix grey-text"></i>
        <input type="email" id="memail_personal" name="memail_personal" class="form-control" required>
        <label for="memail_personal">Email Personal</label>
    </div>