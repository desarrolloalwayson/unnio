<div class="md-form">
    <i class="fa fa-user prefix grey-text"></i>
    <input type="text" id="mnombres" name="mnombres" class="form-control" required>
    <input type="hidden" name="panel" id="panel" value="paneldestinatarios" readonly="readonly">
    <label for="mnombres">Nombres</label>
</div>

<div class="md-form">
    <i class="fa fa-user prefix grey-text"></i>
    <input type="text" id="mapellidos" name="mapellidos" class="form-control" required>
    <label for="mapellidos">Apellidos</label>
</div>
<!-- Material input email -->
<div class="md-form">
    <i class="fa fa-envelope prefix grey-text"></i>
    <input type="email" id="memail" name="memail" class="form-control" required>
    <label for="memail">Email</label>
</div>

<!-- Material input email -->
<div class="md-form">
    <i class="fa fa-phone prefix grey-text"></i>
    <input type="tel" id="mtelefono" name="mtelefono" class="form-control" required>
    <label for="mtelefono">Teléfono</label>
</div>