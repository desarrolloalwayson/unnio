
    <div class="md-form">
        <i class="fa fa-university prefix grey-text active"></i>
        <input type="text" id="mbanco" name="mbanco" value="<?=$datos[0]['banco'];?>" class="form-control" required>
        <input type="hidden" id="mid" name="mid" value="<?=$datos[0]['id'];?>" readonly>
        <label for="mbanco active" class="active">Banco</label>
    </div>

    <div class="md-form">
        <i class="fa fa-id-card-o prefix grey-text active"></i>
        <input type="text" id="mtipo" name="mtipo" value="<?=$datos[0]['tipo'];?>" class="form-control" required>
        <label for="mtipo active" class="active">Tipo de Cuenta</label>
    </div>

    <!-- Material input email -->
    <div class="md-form">
        <i class="fa fa-credit-card prefix grey-text active"></i>
        <input type="text" id="mnumero" name="mnumero" value="<?=$datos[0]['numero'];?>" class="form-control" required>
        <label for="mnumero active" class="active">Número Cuenta Bancaria</label>
    </div>