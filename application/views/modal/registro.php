    <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-user prefix"></i>
        <input type="text" id="mnombre" name="mnombre" class="form-control form-control-sm" required>
        <label style="color: #757575;" for="mnombre">Nombres</label>

        <p id="mensajeErrorNombres"></p>
    </div>

    <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-user prefix"></i>
        <input type="text" id="mapellido" name="mapellido" class="form-control form-control-sm" required>
        <label style="color: #757575;" for="mapellido">Apellidos </label>

        <p id="mensajeErrorApellidos"></p>
    </div>

    <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-id-card-o prefix"></i>
        <input type="text" id="mrut" name="mrut" class="form-control form-control-sm" maxlength="12" required>
        <label style="color: #757575;" for="mrut">RUT </label>
        <p id="mensajeSucessRut" class="sucessgm">El Rut es Valido.</p>
        <p id="mensajeErrorRut" class="errorgm">El Rut es Invalido.</p>
    </div>



         

    <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-envelope prefix"></i>
        <input type="email" id="memail1" name="memail1" class="form-control form-control-sm" required>
        <label style="color: #757575;" for="memail1">Email</label>
        <p id="mensajeErrorEmail"></p>
    </div>

    <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-phone prefix"></i>
        <input type="tel" id="mtelefono" name="mtelefono" class="form-control form-control-sm" required>
        <label style="color: #757575;" for="mtelefono">Teléfono</label>

        <p id="mensajeErrorTelefono"></p>
    </div>

    <!-- Material input subject -->
    <div class="md-form form-sm">
      <i style="margin-top: 0rem;" class="fa fa-lock prefix"></i>
      <input type="password" id="mpassword1" name="mpassword1" onkeyup="comprobarClave();" class="form-control" required>
      <label style="color: #757575;" for="mpassword1">Contraseña</label>
      <p id="mensajeErrorCaracter" class="errorgm">Al menos debe ingresar 4 dígitos.</p>
    </div>

      <div class="md-form form-sm">
        <i style="margin-top: 0rem;" class="fa fa-exclamation-triangle prefix"></i>
        <input type="password" id="mpassword2" name="mpassword2" onkeyup="comprobarClave();" class="form-control" required>
        <label style="color: #757575;" for="mpassword2">Confirmar Contraseña</label>

        <p id="mensajeErrorContrasenaConfirm"></p>
      </div>

    <script type="text/javascript">
      $(function(){
        $("#mrut")
            .rut({formatOn: 'keyup', validateOn: 'keyup'})
            .on('rutInvalido', function(){
                //$(this).parents(".control-group").addClass("error")
                //alert('2');
                $("#mensajeSucessRut").css("display", "none");
                $("#mensajeErrorRut").css("display", "block");
                $('#registrarme').prop('disabled', true);
            })
            .on('rutValido', function(){
                //$(this).parents(".control-group").removeClass("error")
                //alert('1');
                $("#mensajeSucessRut").css("display", "block");
                $("#mensajeErrorRut").css("display", "none");
            });        
      });
    </script>