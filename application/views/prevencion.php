<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Seduc - Educar para servir</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.ico');?>">    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,700,700i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('assets/css/mdb.min.css');?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <style media="screen">
        .dropdown-toggle::after {
            color: #a4cf4e;
        }
        
        .callToAction {
            padding: 20px;
            /* background: #a4cf4e; */
            background: #008fda;
            color: #fff;
            display: flex;
            align-items: center;
        }
        
        .titleSection {
            padding-top: 3rem;
        }
        
        .title {
            left: 36%;
        }
    </style>

</head>

<body>
    <div id="loader-wrapper">
        <div id="loader"></div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>

         <!--Main Navigation-->
    <!-- Sección Header DATOS PERSONALES -->
    <header class="top-container" id="main">
        <div class="container">
            <div class="row ">


                <div class="col-md-5 col-sm-12 ">
                    <div class="logoUno">
                       <img src="<?php echo base_url('assets/img/logo_seduc.png');?>" height="80" widht="80" alt="">
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 text-right pt-1 d-none d-md-block">
                    <span class="tituloDatos">Atención telefónica</span>
                    <br>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>

                <!-- Visible solo para dispositivos móbiles -->
                <div class="col-md-6 col-sm-12 text-center pt-1 d-block d-md-none mt-2">
                    <span class="tituloDatos">Atención telefónica: &nbsp;</span>
                    <span class="numDatos"><i class="fa fa-phone" aria-hidden="true"></i> (+56) 2 2882 1715</span>
                </div>
                <!-- Fin Visible solo para dispositivos móbiles -->

                <div class="col-md-1 text-center">
                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i style="color:#008fda;" class="fa fa-user-circle-o sizeIconUser"></i>
                            </a>

                  <div class="dropdown-menu dropdown-menu-right dropdown-center" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/datos');?>">Datos Personales</a>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/contrasenha');?>">Cambio Contraseña</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('configuracion/logout');?>">Cerrar Sesión</a>
                  </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </header>

    <!-- Sección Navbar/Menú-->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color" data-toggle="affix">
        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
            <ul class="navbar-nav mr-auto menuCentrado">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light smooth-scroll" href="<?php echo base_url('home');?>">
                                Home
                            </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Servicios
                  </a>
                    <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-2">
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#controlParental">Control Parental</a>
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#controlBloqueo">Control y bloqueo de Contactos</a>
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#bloqueoContactos">Bloqueo de contactos en WhatsApp (reactivo)</a>
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#ControlPrivacidad">Control de privacidad</a>
                        <a class="dropdown-item waves-effect waves-light smooth-scroll" href="#GeolocalizacionSeguridad">Geolocalización y seguridad periférica</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link waves-effect waves-light" href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;">
                                Chat Online
                        </a>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Canales
                  </a>
                  <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/callback');?>">Call Back</a>
                    <a class="dropdown-item waves-effect waves-light" href="<?php echo base_url('canales/contacto');?>">Contacto Email</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link waves-effect waves-light" href="<?php echo base_url('cobertura');?>">Detalle Cobertura</a>
                </li>
            </ul>

        </div>
    </nav>

    <!-- Sección Home/Main -->
    <div class="bgHomeHogar">
        <div class="title">
            <p>Conoce tus servicios para la</p>
            <h1>Prevención</h1>
        </div>

        <div class="botonTitle">
            <!-- <button type="button" class="btn btn-primary">Baja y sorpréndete </button> -->
            <a href="#controlParental" class="animated pulse infinite btn-floating blue waves-effect waves-light smooth-scroll">
                <i class="fa fa-arrow-down"></i>
            </a>
        </div>
    </div>

    <!-- Sección Gestor Huella Digital -->
    <section class="container-fluid" id="controlParental">
        <div class="row">
            <div class="col-md-6 imgHuella d-none d-md-block"></div>
            <div class="col-md-6 pt-5 pb-4 marPadBottom50">
                <div class="titleSection">
                    <h2>Control Parental</h2>

                </div>
                <br>
                <p class="px-4">
                    Con este servicio podrás mantener tus dispositivos y redes del hogar de forma segura.
                </p>

                <ul>
                    <li>
                        Asesoría en la instalación de redes inalámbricas.
                    </li>
                    <li>Configuración de dispositivos del hogar.</li>
                    <li>Optimización de las redes inalámbricas.</li>
                </ul>

                <br>


                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center  ">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>

            </div>
            <!-- Visible solo para dispositivos móbiles -->
            <div class="col-md-6 imgLeft d-block d-md-none"></div>
            <!-- Fin Visible solo para dispositivos móbiles -->
        </div>
    </section>

    <!-- INICIO REPUTACIÓN ONLINE -->
    <section class="container-fluid" id="controlBloqueo">
        <div class="row">
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Control y bloqueo de Contactos</h2>

                    <!-- <h4></h4> -->
                </div>
                <br>
                <p class="px-4">Con este servicio podrás tener asesoría en la instalación y configuración de tus nuevos equipos.</p>
                <ul>
                    <li>
                        Instalación y configuración de software.
                    </li>
                    <li>
                        Análisis y limpieza de los dispositivos mejorando su rendimiento y desempeño.
                    </li>
                    <li>
                        Asesoría en la configuración y sincronización de los distintos dispositivos móviles.
                    </li>
                </ul>
                <br>


                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>

            </div>
            <div class="col-md-6 imgReputacion"></div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row callToAction">
            <div class="col-md-9 pl-5 mt-2">
                <h3><strong>¿Necesitas asesoría? ¡Conversemos!</strong></h3>
            </div>
            <div class="col-md-3">
                <a class="btn btn-default" href="tel:+56 2 28821715">
                    <i class="fa fa-phone" aria-hidden="true"></i> <strong>(+56) 2 2882 1715</strong>
                </a>
            </div>
        </div>
    </div>

    <!-- INICIO ASISTENCIA LEGAL -->
    <section class="container-fluid" id="bloqueoContactos">
        <div class="row">
            <div class="col-md-6 imgSeguridad d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Bloqueo de contactos en WhatsApp (reactivo)</h2>
                    <!-- <h4></h4> -->
                </div>
                <br>
                <p class="px-4">Con este servicio podrás proteger tus dispositivos ante posibles vulnerabilidades</p>

                <ul>
                    <li>
                        Asesoría en la configuración de dispositivos para la navegación segura.
                    </li>
                    <li>
                        Protección de virus y malware.
                    </li>
                    <li>
                        Instalación de antivirus y complementos de navegación.
                    </li>
                </ul>
                <br>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>

            </div>
            <div class="col-md-6 bgAsistenciaLegal d-block d-md-none"></div>
        </div>
    </section>


    <!-- INICIO Herencia Digital -->
    <section class="container-fluid" id="ControlPrivacidad">
        <div class="row">

            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Control de privacidad</h2>
                    <!-- <h4></h4> -->
                </div>
                <br>
                <p class="px-4">Con este servicio te asesoraremos en el respaldo de tu información.</p>

                <ul>
                    <li>
                        Asesoría en el respaldo de información en la nube.
                    </li>
                    <li>
                        Asesoría de herramientas para el respaldo de información.
                    </li>
                </ul><br>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>
                <!-- <button type="button" class="btn btn-outline-info waves-effect ml-4"></button> -->

            </div>

            <div class="col-md-6 imgSoporteRespaldo"></div>
        </div>
    </section>

    

        <!-- INICIO ASISTENCIA LEGAL -->
    <section class="container-fluid" id="GeolocalizacionSeguridad">
        <div class="row">
            <div class="col-md-6 imgAlarmaAntirrobo d-none d-md-block"></div>
            <div class="col-md-6 pt-5 marPadBottom50">
                <div class="titleSection">
                    <h2>Geolocalización y seguridad periférica</h2>
                    <!-- <h4></h4> -->
                </div>
                <br>
                <p class="px-4">Con este servicio te asesoraremos en el respaldo de tu información.</p>

                <ul>
                    <li>
                        Asesoría en el respaldo de información en la nube.
                    </li>
                    <li>
                        Asesoría de herramientas para el respaldo de información.
                    </li>
                </ul><br>

                <div class="elementCenter mt-4 ml-4 mr-4">
                    <a href="https://secure.livechatinc.com/licence/2047681/open_chat.cgi?groups=68" target="popup" onclick="window.open(this.href, this.target, 'width=500px,height=500px'); return false;" class="btn btn-primary waves-effect text-center">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> &nbsp; Solicitar vía Chat
                    </a>
                </div>
            </div>
            <div class="col-md-6 bgAsistenciaLegal d-block d-md-none"></div>
        </div>
    </section>


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/mdb.min.js');?>"></script>

    <script>
        $(document).ready(function() {
            $('body').addClass('loaded');
            //Efecto Scroll
            // $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
            //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            //     var target = $(this.hash);
            //     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            //     if (target.length) {
            //       $('html, body').animate({
            //         scrollTop: target.offset().top
            //       }, 1000);
            //       return false;
            //     }
            //   }
            // });

        });
    </script>

    <script>
        $(document).ready(function() {
            var toggleAffix = function(affixElement, scrollElement, wrapper) {
                var height = affixElement.outerHeight(),
                    top = wrapper.offset().top;

                if (scrollElement.scrollTop() >= top) {
                    wrapper.height(height);
                    affixElement.addClass("affix");
                } else {
                    affixElement.removeClass("affix");
                    wrapper.height('auto');
                }
            };
            $('[data-toggle="affix"]').each(function() {
                var ele = $(this),
                    wrapper = $('<div></div>');

                ele.before(wrapper);
                $(window).on('scroll resize', function() {
                    toggleAffix(ele, $(this), wrapper);
                });

                // init
                toggleAffix(ele, $(window), wrapper);
            });

        });
    </script>


    <script>
        // Select all links with hashes
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    </script>

</body>

</html>